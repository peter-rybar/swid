package swid.sp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.*;
import java.security.SecureRandom;
import java.util.*;


/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidSpTest {

	private static final Logger LOG = LoggerFactory.getLogger(SwidSpTest.class);

	@Test(enabled = false)
	public void testValidate() throws Exception {

//		LOG.error("composed invalid IdP URL: {}", "aaa", new Exception());
	}

	@Test
	public void token() {
		SecureRandom secureRandom = new SecureRandom();
		String token = new BigInteger(130, secureRandom).toString(64);
		System.out.println(token);
	}

	@Test(enabled = false)
	public void connectUrl() {
		String params = "action=probe&user=jkovar";
		String urlBase = "http://localhost:8080/swid";
		try {
			String url = urlBase + "?" + params;
			URLConnection urlConnection = new URL(url).openConnection();
			urlConnection.connect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void performance() {
		String urlParams = null;
		String CHARSET = "UTF-8";
		try {
			urlParams = "login=" + URLEncoder.encode("ur&čťe<>#", CHARSET) +
					"&a=jhkljhk";
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
		int c = 10000000;

		try {
			long start1 = System.currentTimeMillis();
			for (int i = 0; i < c; i++) {
				Map<String, List<String>> parsedQuery = parseUrlEncodedData(urlParams, CHARSET);
				if (c == 1) {
					Set<String> keySet = parsedQuery.keySet();
					for (String string : keySet) {
						for (String string2 : parsedQuery.get(string)) {
							System.out.println(string2);
						}
					}
				}
			}
			long end1 = System.currentTimeMillis();
			long time1 = end1 - start1;
			System.out.println("execTime1 - Tokenizer:" + time1);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
	}

	private static Map<String, List<String>> parseUrlEncodedData(
			String urlParams, String encoding)
			throws UnsupportedEncodingException
	{
		LOG.trace("parseUrlEncodedData({}, {})", urlParams, encoding);

		Map<String, List<String>> params = new HashMap<String, List<String>>();
		StringTokenizer pairs = new StringTokenizer(urlParams, "&");
		while (pairs.hasMoreTokens()) {
			String pair = pairs.nextToken();
			StringTokenizer parts = new StringTokenizer(pair, "=");
			String name = URLDecoder.decode(parts.nextToken(), encoding);
			String value = URLDecoder.decode(parts.nextToken(), encoding);
			List<String> values = params.get(name);
			if (values == null) {
				values = new ArrayList<String>();
				params.put(name, values);
			}
			values.add(value);
		}
		return params;
	}

}
