package swid.sp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class CreateValidateUrlException extends Exception {

	private static final long serialVersionUID = 1L;

	public CreateValidateUrlException(String message) {
		super(message);
	}

	public CreateValidateUrlException(String message, Throwable cause) {
		super(message, cause);
	}

	public CreateValidateUrlException(Throwable cause) {
		super(cause);
	}

}
