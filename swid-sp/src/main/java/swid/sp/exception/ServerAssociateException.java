package swid.sp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class ServerAssociateException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServerAssociateException(String message) {
		super(message);
	}

	public ServerAssociateException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServerAssociateException(Throwable cause) {
		super(cause);
	}

}
