package swid.sp.exception;

import swid.sp.model.Swid;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class ValidateException extends Exception {

	private static final long serialVersionUID = 1L;

	private Swid swid;

	public ValidateException(String message) {
		super(message);
	}

	public ValidateException(Swid swid, String message) {
		super(message);
		this.setSwid(swid);
	}

	public Swid getSwid() {
		return swid;
	}

	public void setSwid(Swid swid) {
		this.swid = swid;
	}

}
