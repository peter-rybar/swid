package swid.sp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class InvalidSwidException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidSwidException(String message) {
		super(message);
	}

	public InvalidSwidException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidSwidException(Throwable cause) {
		super(cause);
	}

}
