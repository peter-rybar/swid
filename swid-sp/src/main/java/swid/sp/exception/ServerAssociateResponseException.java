package swid.sp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class ServerAssociateResponseException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServerAssociateResponseException(String message) {
		super(message);
	}

}
