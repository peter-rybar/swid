package swid.sp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swid.sp.exception.InvalidSwidException;
import swid.sp.exception.ServerAssociateException;
import swid.sp.exception.ServerAssociateResponseException;
import swid.sp.exception.ValidateException;
import swid.sp.model.SwidData;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidSpCtrl {

	private static final Logger LOG = LoggerFactory.getLogger(SwidSpCtrl.class);

	private final SwidSp swidSp;

	public SwidSpCtrl(SwidSp.SessionLitener sessionLitener) {
		swidSp = new SwidSp(sessionLitener);
	}

	public String login(String swid, String base, String callback)
			throws InvalidSwidException,
				ServerAssociateException,
				ServerAssociateResponseException
	{
		LOG.debug("SWID SP [UA -> SP] login (1): \n\t swid = '{}'", swid);

		String idpAuthUrl;
		try {

			idpAuthUrl = this.swidSp.associate(swid, base, callback);

			LOG.debug("SWID SP [SP -> UA] login (4): swid = '{}'\n\t redirect UA to IdP auth URL = '{}'",
					swid, idpAuthUrl);

		} catch (InvalidSwidException e) {
			LOG.error("SWID SP [UA -> SP] associate (2): swid = '{}': InvalidSwidException: {}",
					swid, e);
			throw e;
		} catch (ServerAssociateException e) {
			LOG.error("SWID SP [UA -> SP] associate (2): swid = '{}': ServerAssociateException: {}",
					swid, e);
			throw e;
		} catch (ServerAssociateResponseException e) {
			LOG.error("SWID SP [UA -> SP] associate (2): swid = '{}': ServerAssociateResponseException: {}",
					swid, e);
			throw e;
		}

		return idpAuthUrl;
	}

	public SwidData loginCallback(String ticket)
			throws ValidateException
	{
		LOG.debug("SWID SP [UA -> SP] loginCallback (8): \n\t ticket='{}'", ticket);

		SwidData swidData;

		try {
			// SWID SP validate
			swidData = this.swidSp.validate(ticket);

			LOG.debug("SWID SP [SP -> UA] loginCallback (11)");

		} catch (ValidateException e) {
			LOG.debug("SWID SP [SP -> UA] loginCallback (11)");
			LOG.error("ValidateException", e);
			throw e;
		}

		return swidData;
	}

	public String logout(String swid, String callback)
			throws InvalidSwidException
	{
		LOG.debug("SWID SP [SP -> UA] logout (12)");

		String redirectIdpLogoutUrl = swidSp.logout(swid, callback);
		return redirectIdpLogoutUrl;
	}

	public void logoutCallback() {
		LOG.debug("SWID SP [UA -> SP] logout (15)");
	}

}
