package swid.sp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swid.sp.exception.*;
import swid.sp.model.Swid;
import swid.sp.model.SwidData;

import javax.net.ssl.*;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidSp {

	private static final Logger LOG = LoggerFactory.getLogger(SwidSp.class);

	private static final String HTTP = "http";
	private static final String HTTPS = "https";

	private static SSLSocketFactory sslSocketFactory;

	private static HostnameVerifier hostnameVerifier;

	// setup SSL context for https
	static {
		TrustManager[] trustAllCertsTrustManager = new TrustManager[] {
				new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							X509Certificate[] certs, String authType) {
					}
				}
			};
		try {
			SSLContext ctx = SSLContext.getInstance("SSL");//TLS
			ctx.init(null, trustAllCertsTrustManager, new SecureRandom());
			sslSocketFactory = ctx.getSocketFactory();
			//SSLContext.setDefault(ctx);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("can't retrieve SSL context instance", e);
			throw new ExceptionInInitializerError(e);
		} catch (KeyManagementException e) {
			LOG.error("can't init SSL context instance", e);
			throw new ExceptionInInitializerError(e);
		}

		hostnameVerifier = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession sslSession) {
//				if (hostname.equals("localhost")) {
//					return true;
//				}
//				return false;
				return true;
			}
		};
	}

	private static final String CHARSET = "UTF-8";

	private static SecureRandom secureRandom = new SecureRandom();

	public static class SaltAndSwid {
		private final String salt;
		private final String swid;

		public SaltAndSwid(String salt, String swid) {
			this.salt = salt;
			this.swid = swid;
		}

		public String getSalt() {
			return this.salt;
		}

		public String getSwid() {
			return this.swid;
		}

		@Override
		public String toString() {
			return "[salt=" + this.salt + ", swid=" + this.swid + "]";
		}

	}

	public static class Conf {

		private boolean forceHttps = false;

		public boolean isForceSSL() {
			return this.forceHttps;
		}

		public void setForceSSL(boolean forceSSL) {
			this.forceHttps = forceSSL;
		}

	}

	public interface SessionLitener {

		void sessionSaveSaltAndSwid(SaltAndSwid saltAndSwid);

		//FIXME listener
		SaltAndSwid sessionLoadAndRemoveSaltAndSwid();

	}

	private final Conf conf = new Conf();

	private final SessionLitener sessionListener;


	public SwidSp(SessionLitener sessionLitener) {
		this.sessionListener = sessionLitener;
	}

	public Conf getConf() {
		return this.conf;
	}

	/**
	 * Request IdP associaton and obtain IdP auth page URL to UA redirect to.
	 *
	 * @param swidStr
	 *            swid in format 'user@host[:port]'
	 * @param base
	 *            base URL
	 * @param callback
	 *            callback URL to validate user and obtain aditional identity
	 *            data
	 * @return IdP auth page URL to UA redirect to
	 * @throws ServerAssociateException ServerAssociateException
	 * @throws ServerAssociateResponseException ServerAssociateResponseException
	 * @throws InvalidSwidException InvalidSwidException
	 */
	public String associate(String swidStr, String base, String callback)
		throws InvalidSwidException,
			ServerAssociateException,
			ServerAssociateResponseException
	{
		String idpAuthUrl;

		Swid swid = new Swid(swidStr);

		String salt = SwidSp.createSalt();

		try {
			idpAuthUrl = associateByProtocol(HTTPS, swid, base, callback, salt);

		} catch (ServerAssociateResponseException | ServerAssociateException e) {
			LOG.warn("SWID SP [IdP -> SP] (2): \n\t associate failed: swid = '{}' {}",
				e, swidStr, e.getMessage());
			throw e;

		} catch (MalformedURLException e) {
			LOG.warn("SWID SP [IdP -> SP] (2): \n\t associate failed: swid = '{}' {}",
					e, swidStr, e.getMessage());
			throw new ServerAssociateException(e.getMessage(), e);

		} catch (IOException e) {
			LOG.warn("SWID SP [IdP -> SP] (2): \n\t HTTPS failed: swid = '{}' {}",
					e, swidStr, e.getMessage());

			if (getConf().isForceSSL()) {
				String message = "HTTPS association failed: " + e.getMessage();
				throw new ServerAssociateException(message, e);
			}

			LOG.warn("SWID SP [IdP -> SP] (2): \n\t Fall back to HTTP: swid = '{}'",
					swidStr);
			try {
				idpAuthUrl = associateByProtocol(HTTP, swid, base, callback, salt);
			} catch (IOException e1) {
				LOG.warn("SWID SP [IdP -> SP] (2): \n\t HTTP failed: swid = '{}' {}",
						swidStr, e1.getMessage());
				throw new ServerAssociateException(e1);
			}
		}

		LOG.debug("SWID SP [IdP -> SP] (3): \n\t store: salt = '{}', swid = '{}'",
				salt, swidStr);
		sessionListener.sessionSaveSaltAndSwid(new SaltAndSwid(salt, swidStr));

		return idpAuthUrl;
	}

	/**
	 * Validate identity authenticated by IdP ticket and obtain additional
	 * identity data.
	 *
	 * @param ticket
	 *            swid IdP authentication validation ticket
	 * @return additional identity data
	 * @throws ValidateException ValidateException
	 */
	public SwidData validate(String ticket)
		throws ValidateException
	{
		LOG.debug("SWID SP [Sp -> IdP] (8): \n\t ticket = '{}'", ticket);

		if (ticket == null) {
			throw new ValidateException("ticket is null");
		}

		SaltAndSwid swidAndSalt = sessionListener.sessionLoadAndRemoveSaltAndSwid();

		String swidStr = swidAndSalt.getSwid();
		String salt = swidAndSalt.getSalt();
		LOG.debug("SWID SP [Sp -> IdP] (8): \n\t load and remove: salt = '{}', swid = '{}'",
				salt, swidStr);

		Swid swid;
		try {
			swid = new Swid(swidStr);
		} catch (InvalidSwidException e) {
			// should be valid because the swidStr has been stored to session
			// after validation in the associate method
			throw new ValidateException(e.getMessage());
		}

		SwidData swidData;

		try {
			swidData = validateByProtocol(HTTPS, swid, salt, ticket);

		} catch (CreateValidateUrlException e) {
			LOG.warn("SWID SP [SP -> IdP] (9): \n\t validate failed: swid = '{}' {}",
					swidStr, e.getMessage());
			throw new ValidateException(swid, e.getMessage());

		} catch (IOException e) {
			LOG.warn("SWID SP [SP -> IdP] (9): \n\t HTTPS failed: swid = '{}' {}",
					swidStr, e.getMessage());

			if (getConf().isForceSSL()) {
				String message = "HTTPS validation failed: " + e.getMessage();
				throw new ValidateException(swid, message);
			}

			LOG.warn("SWID SP [SP -> IdP] (9): \n\t Fall back to HTTP: swid = '{}'",
					swidStr);
			try {
				swidData = validateByProtocol(HTTP, swid, salt, ticket);
			} catch (Exception e1) {
				LOG.error("SWID SP [SP -> IdP] (9): \n\t HTTP failed: swid = '{}' {}",
						swidStr, e1.getMessage());
				String message = "HTTP validation failed: " + e1.getMessage();
				throw new ValidateException(swid, message);
			}
		}

		LOG.debug("SWID SP [IdP -> SP] (10): \n\t IdP validate response: swid = '{}' data = '{}'",
				swidStr, swidData.getData().toString());
		return swidData;
	}

	public String logout(String swidStr, String callback) //FIXME - DANO - pokracovat v rewiew !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			throws InvalidSwidException
	{
		LOG.debug("SWID SP [SP -> UA] logout (12): \n\t swid = '{}' \n\t callback = '{}'",
				swidStr, callback);

		Swid swid = new Swid(swidStr);

		String redirectIdpLogoutUrl = "http://" + swid.getHost() + "/swid/logout" +
				"?callback=" + urlEncode(callback);

		LOG.debug("SWID SP [SP -> IdP] logout (12): \n\t swid = '{}' \n\t callback = '{}' \n\t redirect IdP logout url = '{}'",
				swidStr, callback, redirectIdpLogoutUrl);

		return redirectIdpLogoutUrl;
	}

	private String associateByProtocol(String protocol, Swid swid, String base,
			String callback, String salt)
		throws ServerAssociateException, ServerAssociateResponseException,
			IOException, MalformedURLException
	{
		String idpUrl = protocol + "://" + swid.getHost() + "/swid";

		String idpAssociateQueryUrlStr = idpUrl + "/associate" +
				"?user=" + urlEncode(swid.getUser()) +
				"&base=" + urlEncode(base) +
				"&callback=" + urlEncode(callback) +
				"&salt=" + urlEncode(salt);

		URL idpAssociateQueryUrl;
		try {
			idpAssociateQueryUrl = new URL(idpAssociateQueryUrlStr);
		} catch (MalformedURLException e) { //FIXME zjednotit vyhadzovane MalformedURLException vinimky CreateValidateUrlException
			LOG.error("composed invalid IdP URL: {}", idpAssociateQueryUrlStr, e);
			throw e;
		}
		LOG.debug("SWID SP [SP -> IdP] (2): \n\t user = '{}'\n\t IdP associate url = '{}'",
				swid.getUser(), idpAssociateQueryUrl);

		Map<String, List<String>> idpAssociateResponseData;
		try {
			idpAssociateResponseData = doHttpGetRequest(idpAssociateQueryUrl);
			LOG.debug("SWID SP [IdP -> SP] (2): \n\t user = '{}'\n\t IdP associate response = '{}'",
					swid.getUser(), idpAssociateResponseData.toString());
		} catch (IOException e) {
			LOG.error("GET request to '{}' failed: {}", idpAssociateQueryUrlStr, e);
			throw e;
		}

//		try {
//			Map<String, List<String>> data = parseUrlEncodedData(idpAuthUrl, CHARSET);
//			if (data.containsKey("error")) {
//				String errorMessage = data.get("error").get(0);
//				throw new ServerAssociateException(errorMessage);
//			}
//		} catch (UnsupportedEncodingException e) {
//			Application.logger().error("SWID SP [IdP -> SP] (3): ", e);
//		}

		if (idpAssociateResponseData.containsKey("error")) {
			String errorMessage = idpAssociateResponseData.get("error").get(0);
			throw new ServerAssociateException(errorMessage);
		}

		String idpAuthUrl;

		if (idpAssociateResponseData.containsKey("auth")) {
			idpAuthUrl = idpAssociateResponseData.get("auth").get(0);
		} else {
			throw new ServerAssociateResponseException("Missing parameter 'auth'");
		}

		return idpAuthUrl;
	}

	private SwidData validateByProtocol(String protocol, Swid swid,
			String salt, String ticket)
		throws CreateValidateUrlException, IOException
	{
		SwidData swidData = new SwidData();

		swidData.setSwid(swid);

		URL validateUrl;
		try {
			validateUrl = createValidateUrl(protocol, swid, salt, ticket);
		} catch (CreateValidateUrlException e) {
			LOG.error("SWID SP [SP -> IdP] (9): \n\t swid = '{}': CreateValidateUrlException: {}",
					swid.asString(), e.getMessage());
			throw e;
		}
		LOG.debug("SWID SP [SP -> Idp] (9): \n\t validation URL = '{}'",
				validateUrl);

		try {
			Map<String, List<String>> idpData = doHttpGetRequest(validateUrl);
			swidData.setData(idpData);
		} catch (IOException e) {
			LOG.error("SWID SP [SP -> IdP] (9): \n\t swid = '{}': IOException: {}",
					swid.asString(), e.getMessage());
			throw e;
		}
		return swidData;
	}

	private Map<String, List<String>> doHttpGetRequest(URL url)
		throws IOException
	{
		LOG.trace("doHttpGetRequest({})", url);

		URLConnection urlConnection = url.openConnection();

		HttpURLConnection httpUrlConnection = (HttpURLConnection) urlConnection;

		if (httpUrlConnection instanceof HttpsURLConnection) {
			HttpsURLConnection httpsUrlConnection;
			httpsUrlConnection = (HttpsURLConnection) httpUrlConnection;
			httpsUrlConnection.setSSLSocketFactory(sslSocketFactory);
			httpsUrlConnection.setHostnameVerifier(hostnameVerifier);
		}

		httpUrlConnection.setDoOutput(false);
		httpUrlConnection.setDoInput(true);

//		httpUrlConnection.connect();
		int responseCode = httpUrlConnection.getResponseCode();
		if (responseCode / 100 != 2) {
			// TODO premysliet aku chybu vyhodit
			// TODO premysliet ako jednotne? riesit error messaging v protokole
			// pri asociacii a validacii
			throw new IOException("http error: " + responseCode
					+ " " + httpUrlConnection.getResponseMessage());
		}

		InputStream inputStream = httpUrlConnection.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		String urlEncodedData = bufferedReader.readLine();

		bufferedReader.close();

		Map<String, List<String>> data = parseUrlEncodedData(urlEncodedData, CHARSET);

		return data;
	}

	private static String urlEncode(String text)
	{
		String encoded = text;
		try {
			encoded = URLEncoder.encode(text, CHARSET);
		} catch (UnsupportedEncodingException ignored) {
		}
		return encoded;
	}

	private static String createSalt() {
		String salt = new BigInteger(130, SwidSp.secureRandom).toString(64);
		return salt;
	}

	private static URL createValidateUrl(String protocol, Swid swid,
			String salt, String ticket)
		throws CreateValidateUrlException
	{
		URL validateUrl;
		try {
			String idpUrl = protocol + "://" + swid.getHost() + "/swid";

			String validateQueryUrl = idpUrl + "/validate" +
					"?salt=" + urlEncode(salt) +
					"&ticket=" + urlEncode(ticket);

			validateUrl = new URL(validateQueryUrl);
		} catch (MalformedURLException e) {
			throw new CreateValidateUrlException(e);
		}
		return validateUrl;
	}

	private static Map<String, List<String>> parseUrlEncodedData(
			String urlParams, String encoding)
		throws UnsupportedEncodingException
	{
		LOG.trace("parseUrlEncodedData({}, {})", urlParams, encoding);
		
		Map<String, List<String>> params = new HashMap<String, List<String>>();
		StringTokenizer pairs = new StringTokenizer(urlParams, "&");
		while (pairs.hasMoreTokens()) {
			String pair = pairs.nextToken();
			StringTokenizer parts = new StringTokenizer(pair, "=");
			String name = URLDecoder.decode(parts.nextToken(), encoding);
			String value = URLDecoder.decode(parts.nextToken(), encoding);
			List<String> values = params.get(name);
			if (values == null) {
				values = new ArrayList<String>();
				params.put(name, values);
			}
			values.add(value);
		}
		return params;
	}

}
