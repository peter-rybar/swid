package swid.sp.model;

import java.util.List;
import java.util.Map;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class SwidData {

	private Swid swid;
	private Map<String, List<String>> data;

	public SwidData(){
	}

	public SwidData(Swid swid, Map<String, List<String>> data){
		setSwid(swid);
		setData(data);
	}

	public Swid getSwid() {
		return swid;
	}

	public void setSwid(Swid swid) {
		this.swid = swid;
	}

	public Map<String, List<String>> getData() {
		return data;
	}

	public void setData(Map<String, List<String>> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SwidData{" +
				"swid=" + swid +
				", data=" + data +
				'}';
	}

}
