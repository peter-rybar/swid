package swid.sp.model;

import swid.sp.exception.InvalidSwidException;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class Swid {

	private String user;
	private String host;

	public Swid(String swid) throws InvalidSwidException {
		if (!swid.contains("@")) {
			throw new InvalidSwidException("swid does not contain character '@'");
		}
		String[] split = swid.split("@");
		if (split.length != 2) {
			throw new InvalidSwidException("swid format must be 'user@host[:port]'");
		}
		this.user = split[0];
		//TODO validate domain
		this.host = split[1];
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * SWID string identifier
	 * @return SWID in form <code>user@host</code>
	 */
	public String asString() {
		return user + "@" + host;
	}

	@Override
	public String toString() {
		return "Swid{" +
				"user='" + user + '\'' +
				", host='" + host + '\'' +
				'}';
	}

}
