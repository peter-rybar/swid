package swid.idp.app.model;

import javax.jdo.annotations.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
@PersistenceCapable
public class User {

	@PrimaryKey()
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;

	@Unique
	String login;

	String name;

	@Unique
	String email;

//	@Persistent(dependent = "true")
	UserCredentials userCredentials;

//	@Element(dependent = "true")
	List<UserData> userData = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserCredentials getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}

	public List<UserData> getUserData() {
		return userData;
	}

	public void setUserData(List<UserData> userData) {
		this.userData = userData;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", login='" + login + '\'' +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", userCredentials=" + userCredentials +
				", userData=" + userData +
				'}';
	}

}
