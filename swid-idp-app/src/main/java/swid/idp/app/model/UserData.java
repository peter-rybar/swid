package swid.idp.app.model;

import javax.jdo.annotations.PersistenceCapable;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
@PersistenceCapable
public class UserData {

	String app;
	String name;
	String value;

	public UserData() {
	}

	public UserData(String app, String name, String value) {
		this.app = app;
		this.name = name;
		this.value = value;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "UserData{" +
				"app='" + app + '\'' +
				", name='" + name + '\'' +
				", value='" + value + '\'' +
				'}';
	}

}
