package swid.idp.app.model;

import javax.jdo.annotations.PersistenceCapable;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
@PersistenceCapable
public class UserCredentials {

	String password;
	String certificate;

	public UserCredentials() {
	}

	public UserCredentials(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	@Override
	public String toString() {
		return "UserCredentials{" +
				"password='" + password + '\'' +
				", certificate='" + certificate + '\'' +
				'}';
	}

}
