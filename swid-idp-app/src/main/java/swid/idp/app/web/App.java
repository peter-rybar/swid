package swid.idp.app.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.jdodn.PM;
import prest.jdodn.TransactionUnit;
import prest.webfw.core.Application;
import prest.webfw.core.ApplicationException;
import prest.webfw.core.Context;
import prest.webfw.web.templates.jade.ViewJadeFilter;
import swid.idp.app.auth.JdoAuthUserListener;
import swid.idp.app.model.User;
import swid.idp.app.model.UserCredentials;
import swid.idp.app.model.UserData;
import swid.idp.app.web.controllers.SwidIdpController;
import swid.idp.listeners.AuthUserListener;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class App extends Application {

	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	@Override
	public void initialize(Context context) throws ApplicationException {
		initPersistence();
		initViewTemplates();

//		AuthUserCallback authUserCallback = new SimpleAuthUserCallback();
		AuthUserListener authUserListener = new JdoAuthUserListener();

		mount("/", new SwidIdpController(authUserListener));
	}

	@Override
	public void destroy() {
		PM.destroy();
	}

	private void initPersistence() throws ApplicationException {
		String propertiesName = "/pm.properties";
		Properties properties = new Properties();
		try {
			InputStream props = getClass().getResourceAsStream(propertiesName);
			properties.load(props);
		} catch (Exception e) {
			LOG.warn("Unable to load '{}': {}", propertiesName, e.getMessage());
			throw new ApplicationException(e);
		}

		PM.init(properties);

		PM.transaction(new TransactionUnit() {
			@Override
			public void transaction(PersistenceManager pm, Transaction tx) {
				for (int i = 0; i < 10; i++) {
					User user = new User();
					user.setLogin("user" + i);
					user.setName("First Last " + i);
					user.setEmail("email" + i);
					UserCredentials userCredentials = new UserCredentials("passwd" + i);
					user.setUserCredentials(userCredentials);
					List<UserData> userData = new ArrayList<>();
					userData.add(new UserData("app" + i, "name" + i, "value" + i));
					user.setUserData(userData);

					User u = pm.makePersistent(user);
					System.out.println(u);
				}
			}

			@Override
			public void error(PersistenceManager pm, Transaction tx, Exception e) {
				LOG.error("transaction error", e);
			}
		});

		PM.transaction(new TransactionUnit() {
			@Override
			public void transaction(PersistenceManager pm, Transaction tx) {
				Query q = pm.newQuery(User.class);
				q.setOrdering("id asc");
				try {
					List<User> results = (List<User>) q.execute();
					if (!results.isEmpty()) {
						for (User user : results) {
							System.out.println(user);
						}
					} else {
						// Handle "no results" case
					}
				} finally {
					q.closeAll();
				}
			}

			@Override
			public void error(PersistenceManager pm, Transaction tx, Exception e) {
				LOG.error("transaction error", e);
			}
		});
	}

	private void initViewTemplates() {
		ViewJadeFilter.setTemplatesPath("/WEB-INF/templates/");
		if (LOG.isDebugEnabled()) {
			ViewJadeFilter.setTemplatesCaching(false);
		} else {
			ViewJadeFilter.setTemplatesPrettyPrint(false);
		}
	}

}
