package swid.idp.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.webfw.core.RedirectException;
import prest.webfw.core.SendErrorException;
import prest.webfw.core.annotations.Action;
import prest.webfw.core.annotations.Key;
import prest.webfw.core.io.Environment;
import prest.webfw.web.WebController;
import prest.webfw.web.templates.jade.ViewJade;
import swid.idp.SwidIdpCtrl;
import swid.idp.app.web.forms.AuthForm;
import swid.idp.exception.AuthRedirectException;
import swid.idp.exception.ValidateException;
import swid.idp.listeners.AuthUserListener;
import swid.idp.listeners.SessionListener;
import swid.idp.model.*;

import javax.servlet.http.HttpSession;

import static prest.webfw.core.http.Method.GET;
import static prest.webfw.core.http.Method.POST;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidIdpController extends WebController {

	private static final Logger LOG = LoggerFactory.getLogger(SwidIdpController.class);

	private SwidIdpCtrl swidIdpCtrl;

	public SwidIdpController(AuthUserListener authUserListener) {
		super();

		SessionListener sessionListenerImpl = new SessionListener() {

			@Override
			public void saveLoggedUser(String login) {
				HttpSession session = Environment.get().getRequest().getSession();
				session.setAttribute("logged_user", login);
			}

			@Override
			public String loadLoggedUser() {
				HttpSession session = Environment.get().getRequest().getSession();
				String loggedUser = (String) session.getAttribute("logged_user");
				return loggedUser;
			}

			@Override
			public void removeLoggedUser() {
				HttpSession session = Environment.get().getRequest().getSession();
				session.removeAttribute("logged_user");
			}

			@Override
			public String loadHash() {
				HttpSession session = Environment.get().getRequest().getSession();
				String hash = (String) session.getAttribute("hash");
				return hash;
			}

			@Override
			public void saveHash(String hash) {
				HttpSession session = Environment.get().getRequest().getSession();
				session.setAttribute("hash", hash);
			}

			@Override
			public void removeHash() {
				HttpSession session = Environment.get().getRequest().getSession();
				session.removeAttribute("hash");
			}

		};

		swidIdpCtrl = new SwidIdpCtrl(authUserListener, sessionListenerImpl);
//		swidIdpCtl.swidIdp.getConf().setUserMandatory(true);
	}

	@Action(method = GET)
	@ViewJade(template = "index.jade")
//	@ViewTh(template = "index.html")
	public void get() {
	}

	@Action(name = "associate", method = GET)
	public String associate(
			@Key("user") String user,
			@Key("base") String base,
			@Key("callback") String callback,
			@Key("salt") String salt)
	{
		String userIP = getRequest().getRemoteAddr();

		String authUrl = getApplicationAbsolutePath() + getControllerPath() +
				"/" + "auth";

		String idpAuthUrlResponse = swidIdpCtrl.associate(
				new AssociateUrlData(user, base, callback, salt),
				userIP, authUrl);

		return idpAuthUrlResponse;
	}

	@Action(name = "auth", method = { GET, POST })
	@ViewJade(template = "auth.jade")
//	@ViewTh(template = "auth.html")
	public AuthForm auth(
			@Key("hash") String hash,
			AuthForm authForm)
		throws RedirectException
	{
		UserAndBase userAndBase;

		if (!authForm.isSubmitted()) {
			authForm = new AuthForm();
		}

		try {

			userAndBase = swidIdpCtrl.auth(
					new AuthUrlData(hash),
					new AuthFormData(
							authForm.isSubmitted(),
							authForm.isValid(),
							authForm.getLogin(),
							authForm.getPassword()));

			authForm.setUser(userAndBase.getUser());
			authForm.setBase(userAndBase.getBase());

		} catch (AuthRedirectException e) {
			throw new RedirectException(e.getRedirectSpCallbackUrl());
		}

		return authForm;
	}

	@Action(name = "validate", method = GET)
	public String validate(
			@Key("salt") String salt,
			@Key("ticket") String ticket)
		throws SendErrorException
	{
		String urlEncodedUserInfoResponse;

		String userIp = getRequest().getRemoteAddr();

		try {

			urlEncodedUserInfoResponse = swidIdpCtrl.validate(
					new ValidateUrlData(salt, ticket),
					userIp);

		} catch (ValidateException e) {
			throw new SendErrorException(e.getHttpStatusCode(),
					e.getHttpStatusMessage(), e);
		}

		return urlEncodedUserInfoResponse;
	}

	@Action(name = "logout", method = GET)
	public void logout(@Key("callback") String callback)
			throws RedirectException
	{
		String redirectSpLogoutCallbackUrl = swidIdpCtrl.logout(callback);

		throw new RedirectException(redirectSpLogoutCallbackUrl);
	}

}
