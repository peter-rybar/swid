package swid.idp.app.web.forms;

import prest.webfw.validators.StringValidator;
import prest.webfw.validators.form.Form;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class AuthForm extends Form {

	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";

	private String user = "";
	private String base = "";

	@Override
	protected void addEntries() {
		addEntry(LOGIN, new StringValidator(false, "", 4, 30), "User login");
		addEntry(PASSWORD, new StringValidator(false, "", 4, 30), "Password");
		addEntrySubmit("submit");
	}

	public String getLogin() {
		return this.getEntryValue(LOGIN);
	}

	public void setLogin(String login) {
		if (login == null) {
			login = "";
		}
		this.setEntryValue(LOGIN, login);
	}

	public String getPassword() {
		return this.getEntryValue(PASSWORD);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

}
