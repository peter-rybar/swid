package swid.idp.app.auth;

import swid.idp.listeners.AuthUserListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class SimpleAuthUserListener implements AuthUserListener {

	@Override
	public boolean userExists(String user, String base) {
		return user.startsWith("user");
	}

	@Override
	public boolean authenticate(String user, String password, String base) {
		return user.startsWith("user") && password.equals(user);
	}

	@Override
	public Map<String, List<String>> userData(String user, String base) {
		Map<String, List<String>> userInfo = new HashMap<>();
		userInfo.put("user", Arrays.asList(user));
		userInfo.put("name", Arrays.asList("User Name"));
		userInfo.put("email", Arrays.asList("user@email.net", "alias@email.net"));
//		userInfo.put("app", Arrays.asList(base));
		return userInfo;
	}

	@Override
	public void logout(String user, String base) {

	}

}
