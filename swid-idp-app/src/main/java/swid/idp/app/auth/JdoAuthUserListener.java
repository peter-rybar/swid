package swid.idp.app.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.jdodn.PM;
import prest.jdodn.TransactionUnit;
import swid.idp.app.model.User;
import swid.idp.app.model.UserCredentials;
import swid.idp.app.model.UserData;
import swid.idp.listeners.AuthUserListener;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.*;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class JdoAuthUserListener implements AuthUserListener {

	private static final Logger LOG = LoggerFactory.getLogger(JdoAuthUserListener.class);

	@Override
	public boolean userExists(String user, String base) {
		User u = loadUser(user);
		return u != null;
	}

	@Override
	public boolean authenticate(String user, String password, String base) {
		User u = loadUser(user);
		if (u != null) {
			UserCredentials userCredentials = u.getUserCredentials();
			String userPassword = userCredentials.getPassword();
			if (userPassword.equals(password)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Map<String, List<String>> userData(String user, String base) {
		Map<String, List<String>> userInfo = new HashMap<>();

		User u = loadUser(user);
		if (u != null) {
			List<UserData> userData = u.getUserData();
			for (UserData data : userData) {
				String app = data.getApp();
				String name = data.getName();
				String value = data.getValue();
				String key = app + "." + name;
				List<String> values;
				if (!userInfo.containsKey(key)) {
					values = new ArrayList<>();
					userInfo.put(key, values);
				} else {
					values = userInfo.get(key);
				}
				values.add(value);
			}
		}
		userInfo.put("user", Arrays.asList(user));
		userInfo.put("login", Arrays.asList(u.getLogin()));
		userInfo.put("name", Arrays.asList(u.getName()));
		userInfo.put("email", Arrays.asList(u.getEmail()));

		return userInfo;
	}

	@Override
	public void logout(String user, String base) {
	}

	private User loadUser(final String userLogin) {
		final User[] user = new User[1];

		PM.transaction(new TransactionUnit() {

			@Override
			public void transaction(PersistenceManager pm, Transaction tx) {
				Query q = pm.newQuery(User.class);
				q.setFilter("login == userLogin");
				q.declareParameters("java.lang.String userLogin");
				try {
					List<User> users = (List<User>) q.execute(userLogin);
					if (!users.isEmpty()) {
						user[0] = users.get(0);
						user[0].getUserCredentials();
						user[0].getUserData();
						user[0] = pm.detachCopy(user[0]);
					}
				} finally {
					q.closeAll();
				}
			}

			@Override
			public void error(PersistenceManager pm, Transaction tx, Exception e) {
				LOG.error("load user error", e);
			}
		});

		return user[0];
	}

}
