package swid.idp.app.auth;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import prest.jdodn.PM;
import prest.jdodn.TransactionUnit;
import swid.idp.app.model.User;
import swid.idp.app.model.UserCredentials;
import swid.idp.app.model.UserData;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class JdoAuthUserListenerTest {

	private static JdoAuthUserListener jdoAuthUserListener;

	@BeforeClass
	public static void setUp() throws Exception {
		setUpDatabase();
		jdoAuthUserListener = new JdoAuthUserListener();
	}

	private static void setUpDatabase() {
		String propertiesName = "/pm.properties";
		Properties properties = new Properties();
		try {
			Class<JdoAuthUserListenerTest> clazz = JdoAuthUserListenerTest.class;
			InputStream props = clazz.getResourceAsStream(propertiesName);
			properties.load(props);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		PM.init(properties);

		PM.transaction(new TransactionUnit() {
			@Override
			public void transaction(PersistenceManager pm, Transaction tx) {
				for (int i = 1; i < 10; i++) {
					User user = new User();
					user.setLogin("user" + i);
					user.setName("First Last " + i);
					user.setEmail("email" + i);
					UserCredentials userCredentials = new UserCredentials("passwd" + i);
					user.setUserCredentials(userCredentials);
					List<UserData> userData = new ArrayList<>();
					userData.add(new UserData("app" + i, "name" + i, "value" + i));
					user.setUserData(userData);

					User u = pm.makePersistent(user);
					System.out.println(">>>> " + u);
				}
			}
		});

		PM.transaction(new TransactionUnit() {
			@Override
			public void transaction(PersistenceManager pm, Transaction tx) {
				Query q = pm.newQuery(User.class);
				q.setOrdering("id asc");
				List<User> results = (List<User>) q.execute();
				if (!results.isEmpty()) {
					for (User user : results) {
						System.out.println(user);
					}
				} else {
					// Handle "no results" case
				}
			}
		});
	}

	@Test
	public void testUserExists() throws Exception {
		boolean result = jdoAuthUserListener.userExists("user3", "");
		Assert.assertEquals(true, result);
	}

	@Test
	public void testAuthenticate() throws Exception {
		boolean result = jdoAuthUserListener.authenticate("user3", "passwd3", "");
		Assert.assertEquals(true, result);
	}

	@Test
	public void testUserData() throws Exception {
		Map<String, List<String>> result = jdoAuthUserListener.userData("user3", "");
//		System.out.println(result);
		Assert.assertEquals(true, !result.isEmpty());
	}

}
