package swid.sp.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.webfw.core.RedirectException;
import prest.webfw.core.annotations.Action;
import prest.webfw.core.io.Request;
import prest.webfw.web.WebController;
import prest.webfw.web.templates.jade.ViewJade;
import swid.sp.app.web.forms.LoginForm;

import static prest.webfw.core.http.Method.GET;
import static prest.webfw.core.http.Method.POST;

/**
 * SWID prest login controller example implementation
 *
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidSpProxyController extends WebController {

	private static final Logger LOG = LoggerFactory.getLogger(SwidSpProxyController.class);

	private static final String LOGIN = "login";

	@Action(name = LOGIN, method = {GET, POST})
	@ViewJade(template = "login.jade")
//	@ViewTh(template = "login.html")
	public LoginForm login(LoginForm loginForm)
		throws RedirectException
	{
		if (loginForm.isSubmitted()) {
			if (loginForm.isValid()) {

				String loginUrl = getApplicationAbsolutePath() +
						"swid-servlet" + "/" + "login";
				throw new RedirectException(loginUrl);
			}
		} else {
			loginForm = new LoginForm();
			Request request = getRequest();
			String idp = request.getServerName() + ":8180";// + request.getServerPort();
			String swid = "@" + idp;
			loginForm.setEntryValue("swid", swid);
		}

		return loginForm;
	}

}
