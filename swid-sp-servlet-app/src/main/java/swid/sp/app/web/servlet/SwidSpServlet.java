package swid.sp.app.web.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swid.sp.SwidSp;
import swid.sp.SwidSpCtrl;
import swid.sp.app.web.model.SwidUser;
import swid.sp.exception.InvalidSwidException;
import swid.sp.exception.ServerAssociateException;
import swid.sp.exception.ServerAssociateResponseException;
import swid.sp.exception.ValidateException;
import swid.sp.model.SwidData;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * SWID prest login controller example implementation
 *
 * @author Peter Rybar, pr.rybar@gmail.com
 */
//@WebServlet(name = "SwidSpServlet", value = "/swid-servlet/*", loadOnStartup = 1)
public class SwidSpServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(SwidSpServlet.class);

	/** name, to which the logged user is bound */
	private static final String SESSION_USER_KEY = "SWID_USER";

	/** name, to which the login referer is bound */
	private static final String SESSION_LOGIN_REFERER_KEY = "SWID_LOGIN_REFERER";

	private static final String LOGIN = "login";
	private static final String LOGIN_CALLBACK = "login_cb";
	private static final String LOGOUT = "logout";
	private static final String LOGOUT_CALLBACK = "logout_cb";

	private static final String SWID_SP_SWID = "swid.sp.swid";
	private static final String SWID_SP_SALT = "swid.sp.salt";

	private String sessionUserKey = SESSION_USER_KEY;

	private String defaultLoginReferer;

	@Override
	public void init(ServletConfig config) throws ServletException {
		LOG.info("init " + getClass().getSimpleName());

		String sessionUserKey = config.getInitParameter(SESSION_USER_KEY);
		if (sessionUserKey != null) {
			this.sessionUserKey = sessionUserKey;
		}

		String defaultLoginReferer = config.getInitParameter("DEFAULT_LOGIN_REFERER");
		if (defaultLoginReferer != null) {
			this.defaultLoginReferer = defaultLoginReferer;
		} else {
			this.defaultLoginReferer = config.getServletContext().getContextPath();
		}
	}

	private SwidSpCtrl newSwidSpCtrl(HttpSession session) {
		return new SwidSpCtrl(new SwidSpSessionLitener(session));
//		this.swidSpCtrl.getConf().setForceSSL(true);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		LOG.info("GET: {}", getResource(request));

		switch (getResource(request)) {
			case LOGIN:
				login(request, response);
				break;

			case LOGIN_CALLBACK:
				loginCallback(request, response);
				break;

			case LOGOUT:
				logout(request, response);
				break;

			case LOGOUT_CALLBACK:
				logoutCallback(request, response);
				break;
			default:
				response.sendError(HttpURLConnection.HTTP_NOT_FOUND);
				break;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		LOG.info("POST: {}", getResource(request));

		switch (getResource(request)) {
			case LOGIN:
				login(request, response);
				break;

			default:
				response.sendError(HttpURLConnection.HTTP_NOT_FOUND);
				break;
		}
	}

	void login(HttpServletRequest request, HttpServletResponse response)
			throws IOException
	{
		String swid = request.getParameter("swid");
		String base = getApplicationAbsolutePath(request);
		String callback = getApplicationAbsolutePath(request) +
				request.getServletPath() + "/" + LOGIN_CALLBACK;

		LOG.info("login: {}", swid);

		if (swid == null) {
			response.sendError(HttpURLConnection.HTTP_UNAUTHORIZED, "no swid");

		} else {
			try {
	
				SwidSpCtrl swidSpCtrl = newSwidSpCtrl(request.getSession());
				String idpAuthUrl = swidSpCtrl.login(swid, base, callback);
				setLoginReferrer(request.getSession(), getApplicationAbsolutePath(request) 
						+ request.getParameter("referer"));
				response.sendRedirect(idpAuthUrl);
	
			} catch (InvalidSwidException e) {
				response.sendError(HttpURLConnection.HTTP_UNAUTHORIZED, e.getMessage());
			} catch (ServerAssociateException e) {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
			} catch (ServerAssociateResponseException e) {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
			}
		}
	}


	void loginCallback(HttpServletRequest request, HttpServletResponse response)
			throws IOException
	{
		String ticket = request.getParameter("ticket");

		try {
			SwidSpCtrl swidSpCtrl = newSwidSpCtrl(request.getSession());
			SwidData swidData = swidSpCtrl.loginCallback(ticket);

			SwidUser swidUser = new SwidUser(swidData, "role");

			setLoggedUser(request.getSession(), swidUser);
			followLoginReferer(request.getSession(), response);

		} catch (ValidateException e) {
			response.sendError(HttpURLConnection.HTTP_UNAUTHORIZED);
		}
	}

	void logout(HttpServletRequest request, HttpServletResponse response)
			throws IOException
	{
		SwidUser swidUser = getLoggedUser(request.getSession());

		String swidStr = swidUser.getSwid().asString();
		String callback = getApplicationAbsolutePath(request) +
				request.getServletPath() + "/" + LOGOUT_CALLBACK;

		try {
			// IdP logout
			SwidSpCtrl swidSpCtrl = newSwidSpCtrl(request.getSession());
			String redirectIdpLogoutUrl = swidSpCtrl.logout(swidStr, callback);

			response.sendRedirect(redirectIdpLogoutUrl);

		} catch (InvalidSwidException e) {
			response.sendError(HttpURLConnection.HTTP_UNAUTHORIZED);
		}
	}

	void logoutCallback(HttpServletRequest request, HttpServletResponse response)
			throws IOException
	{
		// SP logout
		setLoggedUser(request.getSession(), null);

		newSwidSpCtrl(request.getSession()).logoutCallback();

		response.sendRedirect(getApplicationAbsolutePath(request));
	}

	private String getResource(HttpServletRequest request) {
		String result = request.getRequestURI().substring(
				request.getContextPath().length());

		result = result.substring(request.getServletPath().length());

		if (result.startsWith("/")) {
			result = result.substring(1);
		}
		if (result.indexOf("/") > -1) {
			result = result.substring(0, result.indexOf("/"));
		}
		return result;
	}

	/**
	 * Sets the specified user in session
	 *
	 * @param user logged user
	 */
	private void setLoggedUser(HttpSession session, SwidUser user) {
		session.setAttribute(sessionUserKey, user);
	}

	private SwidUser getLoggedUser(HttpSession session) {
		return (SwidUser) session.getAttribute(SESSION_USER_KEY);
	}

	/**
	 * @param loginReferrer login referrer url
	 */
	private void setLoginReferrer(HttpSession session, String loginReferrer) {
		LOG.info("saving login referer: {}", loginReferrer);
		session.setAttribute(SESSION_LOGIN_REFERER_KEY, loginReferrer);
	}

	private void followLoginReferer(HttpSession session,
			HttpServletResponse response) throws IOException
	{
		String loginReferer = (String) session.getAttribute(SESSION_LOGIN_REFERER_KEY);
		session.removeAttribute(SESSION_LOGIN_REFERER_KEY);
		if (loginReferer == null) {
			loginReferer = this.defaultLoginReferer;
		}
		
		LOG.info("following login referer: {}", loginReferer);
		response.sendRedirect(loginReferer);
	}

	static String getApplicationAbsolutePath(HttpServletRequest request) {
		String result;

		try {
			URL url = new URL(request.getScheme(), request.getServerName(),
					request.getServerPort(), request.getContextPath());
			if (request.getServerPort() == url.getDefaultPort()) {
				url = new URL(request.getScheme(), request.getServerName(),
						request.getContextPath());
			}
			result = url.toExternalForm();
		} catch (MalformedURLException e) {
			LOG.error("Malformed URL: ", e);
			result = null;
		}

		return result;
	}

	class SwidSpSessionLitener implements SwidSp.SessionLitener {
		
		private HttpSession session;

		/**
		 * Constructor
		 * 
		 */
		public SwidSpSessionLitener(HttpSession session) {
			this.session = session;
		}

		@Override
		public void sessionSaveSaltAndSwid(SwidSp.SaltAndSwid saltAndSwid) {
			session.setAttribute(SWID_SP_SALT, saltAndSwid.getSalt());
			session.setAttribute(SWID_SP_SWID, saltAndSwid.getSwid());

			LOG.trace("sessionSaveSaltAndSwid({}) set to session={}",
					saltAndSwid, session.getId());
			//Cookie cookie = new Cookie(SWID_SP_SWID, swid);//TODO toto sa pouziva?
			//cookie.setMaxAge(365 * 24 * 3600);
			//getResponse().addCookie(cookie);
		}

		@Override
		public SwidSp.SaltAndSwid sessionLoadAndRemoveSaltAndSwid() {
			String sessionSalt = (String) session.getAttribute(SWID_SP_SALT);
			String sessionSwid = (String) session.getAttribute(SWID_SP_SWID);

			session.setAttribute(SWID_SP_SALT, null);
			session.setAttribute(SWID_SP_SWID, null);

			SwidSp.SaltAndSwid result = new SwidSp.SaltAndSwid(sessionSalt, sessionSwid);

			LOG.trace("sessionLoadAndRemoveSaltAndSwid() loaded {} from session={}",
					result, session.getId());

			return result;
		}

	};
}
