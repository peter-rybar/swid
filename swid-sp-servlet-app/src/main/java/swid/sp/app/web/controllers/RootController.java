package swid.sp.app.web.controllers;

import prest.webfw.core.RedirectException;
import prest.webfw.core.annotations.Action;
import prest.webfw.web.WebController;
import prest.webfw.web.access.rbac.RBAC;
import prest.webfw.web.auth.form.FormAuth;
import prest.webfw.web.auth.form.FormAuthFilter;
import prest.webfw.web.templates.jade.ViewJade;
import swid.sp.app.web.model.SwidUser;
import swid.sp.app.web.model.UserData;

import static prest.webfw.core.http.Method.GET;

/**
 * SWID prest login controller example implementation
 *
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class RootController extends WebController {

	private static final String USER = "user";
	private static final String DENY = "deny";

	@Action(method = GET)
	@ViewJade(template = "index.jade")
	public void index() throws RedirectException {
	}

	@Action(name = USER, method = GET)
	@FormAuth
	@RBAC(roles = {"role"})
	@ViewJade(template = "user.jade")
	public UserData user() throws RedirectException {
		UserData userData = new UserData();

		SwidUser swidUser = FormAuthFilter.getLoggedUser();
		userData.setSwidData(swidUser.getSwidData());
		userData.setLogoutPageRedirect(FormAuthFilter.getLogoutPageRedirect());

		return userData;
	}

	@Action(name = DENY, method = GET)
	@ViewJade(template = "deny.jade")
	public void deny() throws RedirectException {
	}

}
