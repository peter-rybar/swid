<?xml version="1.0" encoding="UTF-8"?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	  xmlns:c="http://java.sun.com/jsp/jstl/core"
	  version="2.0">
	<jsp:directive.page contentType="application/xhtml+xml" isErrorPage="true"/>
	<jsp:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
		doctype-root-element="html"
		omit-xml-declaration="false"/>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>500 Internal Server Error</title>
	</head>
	<body>
		<h1>500 Internal Server Error</h1>
		<p>Oops <code>NullPointerException</code>.</p>
		<p>(this is really not for swid.sp.app user, give him some general
            information)</p>
	</body>
	</html>

</jsp:root>
