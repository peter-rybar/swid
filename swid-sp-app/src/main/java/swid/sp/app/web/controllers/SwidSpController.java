package swid.sp.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.webfw.core.RedirectException;
import prest.webfw.core.annotations.Action;
import prest.webfw.core.annotations.Key;
import prest.webfw.core.io.Request;
import prest.webfw.web.WebController;
import prest.webfw.web.auth.form.FormAuthFilter;
import prest.webfw.web.templates.jade.ViewJade;
import swid.sp.SwidSp;
import swid.sp.SwidSpCtrl;
import swid.sp.app.web.forms.LoginForm;
import swid.sp.app.web.model.SwidUser;
import swid.sp.exception.InvalidSwidException;
import swid.sp.exception.ServerAssociateException;
import swid.sp.exception.ServerAssociateResponseException;
import swid.sp.exception.ValidateException;
import swid.sp.model.Swid;
import swid.sp.model.SwidData;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

import static prest.webfw.core.http.Method.GET;
import static prest.webfw.core.http.Method.POST;

/**
 * SWID prest login controller example implementation
 *
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidSpController extends WebController {

	private static final Logger LOG = LoggerFactory.getLogger(SwidSpController.class);

	private static final String LOGIN = "login";
	private static final String LOGIN_CALLBACK = "login_cb";
	private static final String LOGOUT = "logout";
	private static final String LOGOUT_CALLBACK = "logout_cb";

	private static final String SWID_SP_SWID = "swid.sp.swid";
	private static final String SWID_SP_SALT = "swid.sp.salt";

	private SwidSpCtrl swidSpCtrl;

	public SwidSpController() {

		SwidSp.SessionLitener sessionLitener = new SwidSp.SessionLitener() {

			@Override
			public void sessionSaveSaltAndSwid(SwidSp.SaltAndSwid saltAndSwid) {
				HttpSession session = getRequest().getSession(true);
				session.setAttribute(SWID_SP_SALT, saltAndSwid.getSalt());
				session.setAttribute(SWID_SP_SWID, saltAndSwid.getSwid());

				LOG.trace("sessionSaveSaltAndSwid({}) set to session={}",
						saltAndSwid, session.getId());
				//Cookie cookie = new Cookie(SWID_SP_SWID, swid);//TODO toto sa pouziva?
				//cookie.setMaxAge(365 * 24 * 3600);
				//getResponse().addCookie(cookie);
			}

			@Override
			public SwidSp.SaltAndSwid sessionLoadAndRemoveSaltAndSwid() {
				HttpSession session = getRequest().getSession();
				String sessionSalt = (String) session.getAttribute(SWID_SP_SALT);
				String sessionSwid = (String) session.getAttribute(SWID_SP_SWID);

				session.setAttribute(SWID_SP_SALT, null);
				session.setAttribute(SWID_SP_SWID, null);

				SwidSp.SaltAndSwid result = new SwidSp.SaltAndSwid(sessionSalt, sessionSwid);

				LOG.trace("sessionLoadAndRemoveSaltAndSwid() loaded {} from session={}",
						result, session.getId());

				return result;
			}

		};

		this.swidSpCtrl = new SwidSpCtrl(sessionLitener);
//		this.swidSpCtrl.getConf().setForceSSL(true);
	}


	@Action(name = LOGIN, method = {GET, POST})
	@ViewJade(template = "login.jade")
//	@ViewTh(template = "login.html")
	public LoginForm login(LoginForm loginForm)
		throws RedirectException
	{
		if (loginForm.isSubmitted()) {
			if (loginForm.isValid()) {

				String swid = loginForm.getSwid();
				String base = getApplicationAbsolutePath();
				String callback = getApplicationAbsolutePath() +
						getControllerPath() + "/" + LOGIN_CALLBACK;

				try {

					String idpAuthUrl = swidSpCtrl.login(swid, base, callback);

					throw new RedirectException(idpAuthUrl);

				} catch (InvalidSwidException e) {
					loginForm.setErrorMessage(e.getMessage());
				} catch (ServerAssociateException e) {
					loginForm.setErrorMessage(e.getMessage());
				} catch (ServerAssociateResponseException e) {
					loginForm.setErrorMessage(e.getMessage());
				}
			}
		} else {
			loginForm = new LoginForm();
			Request request = getRequest();
			String idp = request.getServerName() + ":8180";// + request.getServerPort();
			String swid = "@" + idp;
			loginForm.setEntryValue("swid", swid);
		}

		return loginForm;
	}


	@Action(name = LOGIN_CALLBACK, method = GET)
	public void loginCallback(@Key("ticket") String ticket)
			throws RedirectException
	{
		try {
			SwidData swidData = swidSpCtrl.loginCallback(ticket);

			Swid swid = swidData.getSwid();
			Map<String, List<String>> data = swidData.getData();

			SwidUser swidUser = new SwidUser(swidData, "role");

			FormAuthFilter.setLoggedUser(swidUser);
			FormAuthFilter.followLoginReferrer();
		} catch (ValidateException e) {
			String accessDeniedUrl = getApplicationPath() + "/deny";
			throw new RedirectException(accessDeniedUrl);
		}
	}

	@Action(name = LOGOUT, method = GET)
	public void logout()
			throws RedirectException
	{
		SwidUser swidUser = (SwidUser)FormAuthFilter.getLoggedUser();

		String swidStr = swidUser.getSwid().asString();
		String callback = getApplicationAbsolutePath() +
				getControllerPath() + "/" + LOGOUT_CALLBACK;

		try {
			// IdP logout
			String redirectIdpLogoutUrl = swidSpCtrl.logout(swidStr, callback);

			throw new RedirectException(redirectIdpLogoutUrl);

		} catch (InvalidSwidException e) {
			//FIXME what to do
			e.printStackTrace();
			String accessDeniedUrl = getApplicationPath() + "/deny";
			throw new RedirectException(accessDeniedUrl);
		}
	}

	@Action(name = LOGOUT_CALLBACK, method = GET)
	public void logoutCallback()
			throws RedirectException
	{
		// SP logout
		FormAuthFilter.unsetLoggedUser();

		swidSpCtrl.logoutCallback();

		throw new RedirectException(getApplicationAbsolutePath());
	}

}
