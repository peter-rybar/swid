package swid.sp.app.web.forms;

import prest.webfw.validators.StringValidator;
import prest.webfw.validators.form.Form;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class LoginForm extends Form {

	private static final String SWID = "swid";
	private String errorMessage = "";

	@Override
	protected void addEntries() {
		addEntry(SWID, new StringValidator(false, "", 4, 30), SWID);
		addEntrySubmit("submit");
	}

	public String getSwid() {
		return this.getEntryValue(SWID);
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
