package swid.sp.app.web.model;

import swid.sp.model.SwidData;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class UserData {

	private SwidData swidData;
	private String logoutPageRedirect;

	public SwidData getSwidData() {
		return this.swidData;
	}

	public void setSwidData(SwidData swidData) {
		this.swidData = swidData;
	}

	public String getLogoutPageRedirect() {
		return logoutPageRedirect;
	}

	public void setLogoutPageRedirect(String logoutPageRedirect) {
		this.logoutPageRedirect = logoutPageRedirect;
	}

	@Override
	public String toString() {
		return "UserData{" +
				"swidData=" + swidData +
				", logoutPageRedirect='" + logoutPageRedirect + '\'' +
				'}';
	}

}
