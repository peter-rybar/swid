package swid.sp.app.web.model;

import prest.webfw.core.io.User;
import swid.sp.model.Swid;
import swid.sp.model.SwidData;

import java.util.Set;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidUser extends User {

	private static final long serialVersionUID = 1L;

	private Swid swid;
	private SwidData swidData;

	public SwidUser(SwidData swidData, String... roles) {
		super(swidData.getSwid().getUser(), null, roles);
		this.swid = swidData.getSwid();
		this.swidData = swidData;
	}

	public SwidUser(SwidData swidData, Set<String> roles) {
		this(swidData, roles.toArray(new String[roles.size()]));
	}

	public Swid getSwid() {
		return swid;
	}

	public void setSwid(Swid swid) {
		this.swid = swid;
	}

	public SwidData getSwidData() {
		return this.swidData;
	}

	public void setSwidData(SwidData swidData) {
		this.swidData = swidData;
	}

	@Override
	public String toString() {
		return "SwidUser{" +
				"swid=" + swid +
				", swidData=" + swidData +
				'}';
	}

}
