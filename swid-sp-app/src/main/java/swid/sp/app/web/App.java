package swid.sp.app.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prest.webfw.core.Application;
import prest.webfw.core.ApplicationException;
import prest.webfw.core.Context;
import prest.webfw.web.access.rbac.RBACFilter;
import prest.webfw.web.auth.form.FormAuthFilter;
import prest.webfw.web.templates.jade.ViewJadeFilter;
import swid.sp.app.web.controllers.RootController;
import swid.sp.app.web.controllers.SwidSpController;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class App extends Application {

	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	@Override
	public void initialize(Context context) throws ApplicationException {
		initAuth();
		initViewTemplates();

		mount("/", new RootController());
		mount("/swid", new SwidSpController());
	}

	@Override
	public void destroy() {
	}

	private void initAuth() {
		FormAuthFilter.setLoginPageRedirect(getApplicationPath() + "/swid/login");
		FormAuthFilter.setLoginReferrerDefault(getApplicationPath());
		FormAuthFilter.setLogoutPageRedirect(getApplicationPath() + "/swid/logout");

		RBACFilter.setGlobalRedirect(getApplicationPath() + "/deny");
	}

	private void initViewTemplates() {
//		ViewThFilter.setTemplatesPrefix("/WEB-INF/templates/");
		ViewJadeFilter.setTemplatesPath("/WEB-INF/templates/");
		if (!LOG.isDebugEnabled()) {
			ViewJadeFilter.setTemplatesPrettyPrint(false);
		}
	}

}
