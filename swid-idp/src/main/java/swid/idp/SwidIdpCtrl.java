package swid.idp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swid.idp.exception.AuthRedirectException;
import swid.idp.exception.ValidateException;
import swid.idp.listeners.AuthUserListener;
import swid.idp.listeners.SessionListener;
import swid.idp.model.*;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidIdpCtrl {

	private static final Logger LOG = LoggerFactory.getLogger(SwidIdpCtrl.class);

	private SwidIdp swidIdp;

	private AuthUserListener authUserListener;

	private SessionListener sessionListener;

	public SwidIdpCtrl(AuthUserListener authUserListener,
					   SessionListener sessionListener) {
		this.swidIdp = new SwidIdp();
		this.authUserListener = authUserListener;
		this.sessionListener = sessionListener;
	}

	public String associate(
			AssociateUrlData associateUrlData,
			String userIP, String authUrl)
	{
		if (LOG.isDebugEnabled()) {
			LOG.debug("SWID IdP [SP -> IdP] associate (2): " +
							"\n\t user = '{}'" +
							"\n\t base = '{}'" +
							"\n\t callback = '{}'" +
							"\n\t salt = '{}'",
					associateUrlData.getUser(),
					associateUrlData.getBase(),
					associateUrlData.getCallback(),
					associateUrlData.getSalt());
		}

		String idpAuthUrlResponse = swidIdp.associate(
				associateUrlData, authUrl, userIP);

		if (LOG.isDebugEnabled()) {
			LOG.debug("SWID IdP [IdP -> SP] associate (2): " +
							"\n\t user = '{}'" +
							"\n\t response: IdP auth URL = '{}'",
					associateUrlData.getUser(), idpAuthUrlResponse);
		}
		return idpAuthUrlResponse;
	}

	public UserAndBase auth(AuthUrlData authUrlData, AuthFormData authFormData)
			throws AuthRedirectException
	{
		String hash = authUrlData.getHash();

		LOG.debug("SWID IdP [UA -> IdP] auth (5, 6): \n\t hash = '{}'", hash);

		if (hash != null) {
			//
			sessionListener.saveHash(hash);
		} else {
			hash = sessionListener.loadHash();
		}

		if (hash == null) {
			return new UserAndBase("", "", "");
		}

		UserAndBase userAndBase = swidIdp.authGetUserAndBase(hash);

		if (userAndBase == null) {
			return new UserAndBase("", "", "");
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("SWID IdP [UA -> IdP] auth (6): " +
							"\n\t userId = '{}'" +
							"\n\t user = '{}'" +
							"\n\t base = '{}'",
					userAndBase.getUserId(),
					userAndBase.getUser(),
					userAndBase.getBase());
		}

		String user = userAndBase.getUser();
		String base = userAndBase.getBase();

		if (authFormData.isSubmitted()) {
			if (authFormData.isValid()) {

				//TODO allow_auth_for_app(base)

				String login;
				if (user.isEmpty()) {
					login = authFormData.getLogin();
				} else {
					login = user;
				}

				String password = authFormData.getPassword();

//				boolean userExists = authUserListener.userExists(login, base);
				boolean authOk = authUserListener.authenticate(login, password, base);

				String redirectSpCallbackUrl;
//				if (userExists && authOk) {
				if (authOk) {
					if (user.isEmpty()) {
						redirectSpCallbackUrl = swidIdp.authSuccessRedirectUrl(hash, login);
					} else {
						redirectSpCallbackUrl = swidIdp.authSuccessRedirectUrl(hash);
					}
					sessionListener.saveLoggedUser(login);
				} else {
					if (user.isEmpty()) {
						redirectSpCallbackUrl = swidIdp.authRejectedRedirectUrl(hash);
					} else {
						redirectSpCallbackUrl = swidIdp.authRejectedRedirectUrl(hash);
					}
				}

				if (LOG.isDebugEnabled()) {
					LOG.debug("SWID IdP [Idp -> UA -> SP] auth (7): " +
									"\n\t redirect SP callback URL = '{}'",
							redirectSpCallbackUrl);
				}

				if (redirectSpCallbackUrl != null) {
					sessionListener.removeHash();
					throw new AuthRedirectException("auth ok", redirectSpCallbackUrl);
				}
			}
		} else {
			String loggedUser = sessionListener.loadLoggedUser();

			if (!user.isEmpty()) {
				if (user.equals(loggedUser)) { // user already logged
					String redirectSpCallbackUrl = swidIdp.authSuccessRedirectUrl(hash, null);

					//TODO confirm by user to send auth OK to SP app (base)

					if (LOG.isDebugEnabled()) {
						LOG.debug("SWID IdP [IdP -> UA -> SP] auth (7): " +
										"\n\t user '{}' already logged" +
										"\n\t redirect SP callback URL = '{}'",
								loggedUser, redirectSpCallbackUrl);
					}
					sessionListener.removeHash();
					throw new AuthRedirectException("auth ok", redirectSpCallbackUrl);
//				} else {
//					authData = new AuthData();
				}
			} else {
				if (loggedUser != null && !loggedUser.isEmpty()) { // user already logged
					String redirectSpCallbackUrl = swidIdp.authSuccessRedirectUrl(hash, loggedUser);

					//TODO confirm by user to send auth OK to SP app (base)

					if (LOG.isDebugEnabled()) {
						LOG.debug("SWID IdP [IdP -> UA -> SP] auth (7): " +
										"\n\t user '{}' already logged" +
										"\n\t redirect SP callback URL = '{}'",
								loggedUser, redirectSpCallbackUrl);
					}
					sessionListener.removeHash();
					throw new AuthRedirectException("auth ok", redirectSpCallbackUrl);
//				} else {
//					authData = new AuthData();
				}
			}
		}

		return userAndBase;
	}

	public String validate(ValidateUrlData validateUrlData, String userIp)
			throws ValidateException
	{
		if (LOG.isDebugEnabled()) {
			LOG.debug("SWID IdP [SP -> IdP] validate (9): " +
							"\n\t salt = '{}'" +
							"\n\t ticket = '{}'",
					validateUrlData.getSalt(),
					validateUrlData.getTicket());
		}

		String urlEncodedUserInfoResponse = null;

		try {

			urlEncodedUserInfoResponse = swidIdp.validate(validateUrlData,
					userIp, authUserListener);

		} catch (ValidateException e) {
			LOG.error("SWID IdP validate (10): validate exception '{}'",
					e.getHttpStatusMessage());
			throw e;
		}

		LOG.debug("SWID IdP [Idp -> SP] validate (10): \n\t response = '{}'",
				urlEncodedUserInfoResponse);

		return urlEncodedUserInfoResponse;
	}

	public String logout(String callback) {
		LOG.debug("SWID IdP [UA -> IdP] logout (13): \n\tcallback = '{}'",
				callback);

		sessionListener.removeLoggedUser();

		LOG.debug("SWID IdP [IdP -> UA] logout (14): \n\tcallback = '{}'",
				callback);

		return callback;
	}

}
