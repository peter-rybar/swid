package swid.idp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swid.idp.exception.ValidateException;
import swid.idp.listeners.AuthUserListener;
import swid.idp.model.*;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//FIXME all key identifiers use to store - salt, ticket, token - have to be UUID !!!!!
/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class SwidIdp {

	private static final Logger LOG = LoggerFactory.getLogger(SwidIdp.class);

	private static SecureRandom secureRandom = new SecureRandom();

	private static final String DIGEST_ALGORITHM = "SHA1";// "MD5"
	private static final String CHARSET = "UTF-8";
	private static final int TIME_TO_LIVE = 60;
	private static final int EXPIRATION_INTERVAL = 60;

	// TODO spravit listener...
	private Map<String, String> salts = new ExpiringMap<String, String>(TIME_TO_LIVE, EXPIRATION_INTERVAL);

	private Map<String, UserBaseCallbackSalt> hashes  = new ExpiringMap<String, UserBaseCallbackSalt>(TIME_TO_LIVE, EXPIRATION_INTERVAL);

	private Map<String, HashAndUser> tickets = new ExpiringMap<String, HashAndUser>(TIME_TO_LIVE, EXPIRATION_INTERVAL);

	private Conf conf = new Conf();

	public static class Conf {

		private boolean userMandatory = false;

		public boolean isUserMandatory() {
			return userMandatory;
		}

		public void setUserMandatory(boolean userMandatory) {
			this.userMandatory = userMandatory;
		}

	}

	public SwidIdp() {
	}

	public Conf getConf() {
		return conf;
	}

	/**
	 * @param associateUrlData
	 * 		user: User login,
	 * 		base: Application Base,
	 * 		callback: Callback URL,
	 * 		salt Salt
	 * @param authUrl Auth URL
	 * @param userIP User IP
	 * @return idpAuthUrlResponse
//	 * @throws AssociateException AssociateException
	 */
	public String associate(
			AssociateUrlData associateUrlData,
			String authUrl, String userIP)
	{
		String user = associateUrlData.getUser();
		String base = associateUrlData.getBase();
		String callback = associateUrlData.getCallback();
		String salt = associateUrlData.getSalt();

		String idpAuthUrlResponse = null;

		if (user == null) {
			user = "";
		}
		if (base == null || callback == null || salt == null ||
				authUrl == null || userIP == null) {
			idpAuthUrlResponse = "error=" + urlEncode("invalid input parameters");
			return idpAuthUrlResponse;
		}

		if (getConf().isUserMandatory() && user.isEmpty()) {
			idpAuthUrlResponse = "error=" + urlEncode("user parameter is mandatory");
			return idpAuthUrlResponse;
		}

		String userId;
		if (user.isEmpty()) { // need some user id for unknown user
			userId = createSalt();
		} else {
			userId = user;
		}

		String idpSalt = createSalt();
		salts.put(salt, idpSalt);

		String hash = hash(userId, salt, idpSalt, userIP);

		hashes.put(hash, new UserBaseCallbackSalt(userId, user, base, callback, salt));

		String redirectIdpAuthUrl = authUrl + "?hash=" + urlEncode(hash);
		idpAuthUrlResponse = "auth=" + urlEncode(redirectIdpAuthUrl);

		return idpAuthUrlResponse;
	}

	/**
	 * @param hash hash
	 * @return user and base
	 */
	public UserAndBase authGetUserAndBase(String hash)
	{
		if (hash == null) {
			return null;
		}
		UserBaseCallbackSalt userBaseCallbackSalt = hashes.get(hash);
		if (userBaseCallbackSalt == null) {
			return null;
		}
		String userId = userBaseCallbackSalt.getUserId();
		String user = userBaseCallbackSalt.getUser();
		String base = userBaseCallbackSalt.getBase();
		return new UserAndBase(userId, user, base);
	}

	/**
	 * @param hash hash
	 * @return redirect SP callback URL
	 */
	public String authSuccessRedirectUrl(String hash)
	{
		return authSuccessRedirectUrl(hash, null);
	}

	/**
	 * @param hash hash
	 * @param loggedUser loggedUser
	 *            logged user if SP not specified user
	 * @return redirect SP callback URL
	 */
	public String authSuccessRedirectUrl(String hash, String loggedUser)
	{
		if (hash == null) {
			return null;
		}

		UserBaseCallbackSalt userBaseCallbackSalt = hashes.get(hash);
		hashes.remove(hash);
		String userId = userBaseCallbackSalt.getUserId();
		String user = userBaseCallbackSalt.getUser();
		String base = userBaseCallbackSalt.getBase();
		if (user.isEmpty() && loggedUser != null) {
			user = loggedUser;
		}
		String callback = userBaseCallbackSalt.getCallback();

		String redirectSpCallbackUrl = callback;

		String ticket = createTicket();

		tickets.put(ticket, new HashAndUser(hash, userId, user, base));

		if (callback.contains("?")) {
			redirectSpCallbackUrl += "&" + "ticket=" + urlEncode(ticket);
		} else {
			redirectSpCallbackUrl += "?" + "ticket=" + urlEncode(ticket);
		}

		return redirectSpCallbackUrl;
	}

	/**
	 * @param hash hash
	 * @return redirect SP callback URL
	 */
	public String authRejectedRedirectUrl(String hash)
	{
		if (hash == null) {
			return null;
		}

		UserBaseCallbackSalt userBaseCallbackSalt = hashes.get(hash);
		hashes.remove(hash);

		String callback = userBaseCallbackSalt.getCallback();

		String redirectSpCallbackUrl = callback;

		if (callback.contains("?")) {
			redirectSpCallbackUrl += "&" + "ticket=";
		} else {
			redirectSpCallbackUrl += "?" + "ticket=";
		}

		return redirectSpCallbackUrl;
	}

	/**
	 * @param validateUrlData
	 * 		salt: Salt,
	 * 		ticket: Ticket
	 * @param userIP User IP
	 * @param authUserListener User info callback
	 * @return url encoded user data
	 * @throws ValidateException ValidateException
//	 * @throws UnsupportedEncodingException UnsupportedEncodingException
//	 * @throws NoSuchAlgorithmException NoSuchAlgorithmException
	 */
	public String validate(ValidateUrlData validateUrlData,
			String userIP, AuthUserListener authUserListener)
		throws ValidateException
	{
		String salt = validateUrlData.getSalt();
		String ticket = validateUrlData.getTicket();

		String serverSalt;
		if (!salts.containsKey(salt)) {
			throw new ValidateException(HttpURLConnection.HTTP_FORBIDDEN,
					"Forbidden - salt is malformed");
		}
		serverSalt = salts.get(salt);
		salts.remove(salt);

		if (!tickets.containsKey(ticket)) {
			throw new ValidateException(HttpURLConnection.HTTP_NOT_FOUND,
					"Not Found - ticket is malformed");
		}
		HashAndUser hashAndUser = tickets.get(ticket);
		String hashValid = hashAndUser.getHash();
		String userId = hashAndUser.getUserId();
		String user = hashAndUser.getUser();
		String base = hashAndUser.getBase();

		if (user == null || hashValid == null) {
			tickets.remove(ticket);
			throw new ValidateException(HttpURLConnection.HTTP_NOT_FOUND,
					"Not Found - no user or invalid hash");
		}

		String hash = hash(userId, salt, serverSalt, userIP);

		if (!hash.equals(hashValid)) {
			tickets.remove(ticket);
			throw new ValidateException(HttpURLConnection.HTTP_FORBIDDEN,
					"Forbidden - invalid hash");
		}
		Map<String, List<String>> userData = authUserListener.userData(user, base);

		if (userData == null) {
			tickets.remove(ticket);
			throw new ValidateException(HttpURLConnection.HTTP_INTERNAL_ERROR,
					"Error - profile ID is not set");
		} else {
			// just to prevent cheating
			userData.put("user", Arrays.asList(user));
		}

		tickets.remove(ticket);

		String urlEncodedUserData = urlEncodeUserData(userData);

		return urlEncodedUserData;
	}

	private static String urlEncode(String text)
	{
		String encoded = text;
		try {
			encoded = URLEncoder.encode(text, CHARSET);
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError(e);
		}
		return encoded;
	}

	// TODO preverit bezpecnost
	private static String createSalt() {
		String salt = new BigInteger(130, SwidIdp.secureRandom).toString(64);
		return salt;
	}

	private static String createTicket() {
		String ticket = new BigInteger(130, SwidIdp.secureRandom).toString(64);
		return ticket;
	}

	private static String hash(String userId, String salt,
		String serverSalt, String userIP)
	{
		return sha1Digest(userId + salt + serverSalt + userIP);
	}

	private static String sha1Digest(String hash)
	{
		byte[] digest = null;

		try {
			byte[] bytesOfMessage = hash.getBytes(CHARSET);
			MessageDigest md = MessageDigest.getInstance(DIGEST_ALGORITHM);
			digest = md.digest(bytesOfMessage);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}

		BigInteger bigInt = new BigInteger(1, digest);
		String hashtext = String.format("%0" + (digest.length << 1) + "x", bigInt);

		return hashtext;
	}

	private static String urlEncodeUserData(Map<String, List<String>> userInfo)
	{
		String result = "";
		boolean first = true;
		for (String key : userInfo.keySet()) {
			List<String> values = userInfo.get(key);
			for (String value : values) {
				if (first) {
					first = false;
				} else {
					result += "&";
				}
				result += urlEncode(key) + "=" + urlEncode(value);
			}
		}
		return result;
	}

}
