package swid.idp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class ValidateException extends Exception {

	private static final long serialVersionUID = 1L;

	private int httpStatusCode;
	private String httpStatusMessage;

	public ValidateException(int httpStatusCode, String httpStatusMessage) {
		super();
		this.httpStatusCode = httpStatusCode;
		this.httpStatusMessage = httpStatusMessage;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public String getHttpStatusMessage() {
		return httpStatusMessage;
	}

}
