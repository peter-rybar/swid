package swid.idp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class AuthRedirectException extends Exception {

	private static final long serialVersionUID = 1L;

	String redirectSpCallbackUrl;

	public AuthRedirectException(String message, String redirectSpCallbackUrl) {
		super(message);
		this.redirectSpCallbackUrl = redirectSpCallbackUrl;
	}

	public String getRedirectSpCallbackUrl() {
		return redirectSpCallbackUrl;
	}

	public void setRedirectSpCallbackUrl(String redirectSpCallbackUrl) {
		this.redirectSpCallbackUrl = redirectSpCallbackUrl;
	}

}
