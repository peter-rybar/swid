package swid.idp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class AssociateException extends Exception {

	private static final long serialVersionUID = 1L;

	public AssociateException(String message) {
		super(message);
	}

}
