package swid.idp.exception;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class LogoutException extends Exception {

	private static final long serialVersionUID = 1L;

	public LogoutException(String message) {
		super(message);
	}

}

