package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class AuthFormData {

	private boolean submitted = false;
	private boolean valid = false;

	private String login = "";
	private String password = "";

//	private String user;
//	private String base;

//	public AuthData() {
//	}

	public AuthFormData(boolean submitted, boolean valid, String login, String password) {
		this.submitted = submitted;
		this.valid = valid;
		this.login = login;
		this.password = password;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public void setUser(String user) {
//		this.user = user;
//	}
//
//	public String getUser() {
//		return user;
//	}
//
//	public void setBase(String base) {
//		this.base = base;
//	}
//
//	public String getBase() {
//		return base;
//	}

	@Override
	public String toString() {
		return "AuthFormData{" +
				"submitted=" + submitted +
				", valid=" + valid +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				'}';
	}

}
