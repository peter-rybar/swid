package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class UserAndBase {

	private String userId;
	private String user;
	private String base;

	public UserAndBase(String userId, String user, String base) {
		this.userId = userId;
		this.user = user;
		this.base = base;
	}

	public String getUserId() {
		return userId;
	}

	public String getUser() {
		return user;
	}

	public String getBase() {
		return base;
	}

	@Override
	public String toString() {
		return "UserAndBase{" +
				"userId='" + userId + '\'' +
				", user='" + user + '\'' +
				", base='" + base + '\'' +
				'}';
	}

}
