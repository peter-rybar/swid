package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class AuthUrlData {

	private String hash;

	public AuthUrlData(String hash) {
		this.hash = hash;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "AuthUrlData{" +
				"hash='" + hash + '\'' +
				'}';
	}

}
