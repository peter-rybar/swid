package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class HashAndUser {

	private String hash;
	private String userId;
	private String user;
	private String base;

	public HashAndUser(String hash, String userId, String user, String base) {
		this.hash = hash;
		this.userId = userId;
		this.user = user;
		this.base = base;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	@Override
	public String toString() {
		return "HashAndUser{" +
				"hash='" + hash + '\'' +
				", userId='" + userId + '\'' +
				", user='" + user + '\'' +
				", base='" + base + '\'' +
				'}';
	}

}
