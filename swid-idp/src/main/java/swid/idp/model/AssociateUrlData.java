package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class AssociateUrlData {

	private String user;
	private String base;
	private String callback;
	private String salt;

	public AssociateUrlData(String user, String base, String callback, String salt) {
		this.user = user;
		this.base = base;
		this.callback = callback;
		this.salt = salt;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Override
	public String toString() {
		return "AssociateUrlData{" +
				"user='" + user + '\'' +
				", base='" + base + '\'' +
				", callback='" + callback + '\'' +
				", salt='" + salt + '\'' +
				'}';
	}

}
