package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public class UserBaseCallbackSalt {

	private String userId;
	private String user;
	private String base;
	private String callback;
	private String salt;

	public UserBaseCallbackSalt(String userId, String user, String base, String callback, String salt) {
		this.userId = userId;
		this.user = user;
		this.base = base;
		this.callback = callback;
		this.salt = salt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUser() {
		return user;
	}

	public String getBase() {
		return base;
	}

	public String getCallback() {
		return callback;
	}

	public String getSalt() {
		return salt;
	}

	@Override
	public String toString() {
		return "UserBaseCallbackSalt{" +
				"userId='" + userId + '\'' +
				", user='" + user + '\'' +
				", base='" + base + '\'' +
				", callback='" + callback + '\'' +
				", salt='" + salt + '\'' +
				'}';
	}

}
