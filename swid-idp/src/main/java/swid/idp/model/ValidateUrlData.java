package swid.idp.model;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public class ValidateUrlData {

	String salt;
	String ticket;

	public ValidateUrlData(String salt, String ticket) {
		this.salt = salt;
		this.ticket = ticket;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Override
	public String toString() {
		return "ValidateUrlData{" +
				"salt='" + salt + '\'' +
				", ticket='" + ticket + '\'' +
				'}';
	}

}
