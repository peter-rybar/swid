package swid.idp.listeners;

import java.util.List;
import java.util.Map;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 *
 */
public interface AuthUserListener {

	boolean userExists(String user, String base);

	boolean authenticate(String user, String password, String base);

	Map<String, List<String>> userData(String user, String base);

	void logout(String user, String base);

}
