package swid.idp.listeners;

/**
 * @author Peter Rybar, pr.rybar@gmail.com
 */
public interface SessionListener {

	void saveHash(String hash);

	String loadHash();

	void removeHash();

	void saveLoggedUser(String login);

	String loadLoggedUser();

	void removeLoggedUser();

}
