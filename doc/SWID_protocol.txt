
SWID - Single Web ID
====================


SWID je komunikačný protokol mezi tromi stranami, které realizujú
autentifikáciu používateľa:

  1) *Poskytovateľ služby* (Service Provider – ďalej len SP).
     To sú stránky, na které sa potrebuje používateľ autentifikovat,
     pretože mu poskytujú istú službu.

  2) *Poskytovateľ identity* (Identity Provider – ďalej len IdP).
     To sú stránky, které spravujú identitu použivateľa, teda vedia kdo je
     a dokážu overiť jeho identitu (napríklad pomocou hesla).

  3) *Webový prehľiadač používateľa* (User Agent – ďalej len UA).

Oproti OpenID alebo aj iným autentifikačným systémom, v protokole SWID
nenájdete žiadne kryptovacie algoritmy, ani žiadne komplikované formáty,
které slúžia k idenifikácii strán. Žádné správy sa ani nepodpisujú,
ani inak špeciálne nešifrujú (okrem bežného šifrovania HTTPS viď ďalej).
To veľmi zjednodušuje implementáciu protokolu na oboch stranách, teda
ako u SP tak aj u IdP. Napriek tomu nič z toho neznižuje zabezpečenie
protokolu proti prípadnému útoku a to ako v snahe autentifikovať sa ako
iný používateľ, tak v snahe získať identifikačné údaje používateľa,
ktorý sa chce autentifikovať.

Celý koncept je založený na dôvere a zdieľaní zodpovednosti.

*SP dôveruje IdP*

SP ani nič iné nezostáva, pretože používateľ si svojho IdP vybral
a teda mu dôveruje. Pokiaľ je IdP nedôverihodný, pravdepodobne
nemá dôveryhodného používateľa. A pretože nebezpečenstvo nedôveryhodného
používateľa je rovnaké ako v prípade použitia známej techniky mena
a hesla, nijako to neznižuje bezpečnosť systému.

*SP overuje IdP jedine podľa domény.*

Vďaka tomu, že zabezpečenie doménových mien sa venuje vo svete značné
úsilie, sú domény považované za relatívně bezpečné. Napríklad používateľ
pochádzajúci z IdP "domain.net" nemôže byť pomocou používateľa z iného
IdP, pretože doména "domain.net" patrí jedine jedinému IdP a ten má
overovanie používateľov veľmi dobre zabezpečené.
Podvodný IdP nemá moc možností presmerovať doménu "domain.net" na seba,
pretože záznamy DNS sú zabezpečené, pomocí DNSSEC, alebo iným zôsobom.

*SP zabezpečuje reláciu pomocou ticket-ov a hash-ov.*

Aby prihlasovacia relácia nebola napadnutá ďalšou stranou, dohovára sa
relácia na troch nezávislých kanáloch, pričom SP z nich vidí dva.
Používa sa tak technika, ktorá sa v poslednej dobe stala veľmi obľúbená
hlavne v bankách a finančných inštitúciách. Na overenie každej transakcie
je používateľovi zaslaná SMS, ktorá obsahuje nejaké unikátne číslo,
ktoré celú transakciu overuje. Príklad s SMS je iba ilustračný,
v skutočnosti sa používa iný kanál, ktorý je vytvorený priamo mezi SP
a IdP bez účasti UA (používateľa). Kanál môže byť voliteľne i šifrovaný
pomocou HTTPS. A práve tento kanál slúži podobne, ako SMS.
SP ju používa práve preto, aby si overil, že používateľ sa na IdP riadne
autentifikoval. Tým odpadá nutnosť komplikovaného systému šifrovania
a podpisovania na strane SP.


Šifrovanie
----------

Bolo spomenuté, že v systéme SWID nie je žiadne šifrovanie, a je tým
myslené, že implementácia systému nevyžaduje skutočne žiadne ďalšie
šifrovacie algoritmy. Avšak šifrovanie nie je úplne zatratené.
Pre zvýšenie bezpečnosti sa doporučuje, aby kanály, které sú určené na
výmenu osobných údajov a overovacích údajov boli šifrované pomocou
štandardného šifrovaného spojenia HTTPS. Takéto šifrovanie dostatočne
postačuje na požadovanú úroven zabezpečenia. Jedná se hlavne o kanály
mezi SP a IdP a mezi UA a IdP.


Popis autentifikácie krok za krokom
-----------------------------------


    SP                                      UA                                      IdP
   -----                                   -----                                   -----
     |                                       |                                       |
     |                                       |                                       |
     | login swid: user@IdP.net              |                                       |
  1. |<--------------------------------------+                                       |
     |                                       |                                       |
     | user: user                            |                                       |
     | base: http://SP.net/                  |                                       |
     | callback: http://SP.net/<login_cb>    |                                       |
     | salt: 649cfd4101 <- (RANDOM)          |                                       |
     |                                       |                                       |
     | associate: http://IdP.net/swid/associate?user=.&base=.&callback=.&salt=.      | SSL/TLS
  2. +---------------------------------------|-------------------------------------->|
     |                                       |                                       | user: user
     |                                       |                                       | base: http://SP.net/
     |                                       |                                       | callback: http://SP.net/<login_cb>
     |                                       |                                       | salt: 649cfd4101
     |                                       |                                       |
     |                                       |                                       | IdP_salt: d64f9c01 <- (RANDOM)
     |                                       |                                       | SAVE {salt: IdP_salt}
     |                                       |                                       |
     |                                       |                                       | user_id = user ? user : (session_id (RANDOM))
     |                                       |                                       |
     |                                       |                                       | hash: hash(user_id, salt, IdP_salt [, IP, ...])
     |                                       |                                       | SAVE {hash: [user_id, user, base, callback, salt]}
     |                                       |                                       |
     |                                       |                                       | IdP auth URL: http://IdP.net/<auth>
     |                                       |                                       | UA redirect URL:
     |                                       |                                       |   OK:  auth=http://IdP.net/swid/<auth>?hash=.
     |                                       |                                       |   ERR: error=error_message
     | replay with URL to IdP auth form      |                                       |
     | associate response OK: auth=http://IdP.net/<auth>?hash=.                      | SSL/TLS
     |                    ERR: error=error_message                                   |
  3. |<--------------------------------------|---------------------------------------+
     |                                       |                                       |
     | SAVE {salt: swid}                     |                                       |
     |                                       |                                       |
     | redirect UA to IdP auth (URL from 3)  |                                       |
     | http://IdP.net/<auth>?hash=.          |                                       |
  4. +-------------------------------------->|                                       |
     |                                       |                                       |
.....|.......................................|.......................................|......................................................................
     |                                       |                                       |
     |                                       | request Auth form                     |
     |                                       | http://IdP.net/<auth>?hash=.          | SSL/TLS
  5. |                                       +-------------------------------------->|
     |                                       |                                       | hash: 62daabc8b31d
     |                                       |                                       |
     |                                       |                                       | LOAD   {hash: [user_id, user, base, callback, salt]} ->
     |                                       |                                       |     user_id: user_id
     |                                       |                                       |     user: user
     |                                       |                                       |     base: http://SP.net/
     |                                       |                                       |     callback: http://SP.net/<login_cb>
     |                                       |                                       |     salt: 62daabc8b31d
     |                                       |                                       | REMOVE {hash: [user_id, user, base, callback, salt]}
  6. |                                       | Auth: user (password, cert, ...)      |
     |                                       |<------------------------------------->| SSL/TLS
     |                                       |                                       |
     |                                       |                                       | allow_auth_for_app(base) -> OK
     |                                       |                                       | auth(user, password) OR user still logged -> OK
     |                                       |                                       |
     |                                       |                                       | ticket: e859359ba90c4 <- (RANDOM)
     |                                       |                                       |
     |                                       |                                       | SAVE {ticket: [hash, user_id, user, base]}
     |                                       |                                       |
     |                                       |                                       | UA redirect SP callback URL: http://SP.net/<login_cb>?ticket=.
  7. |                                       | redirect UA to SP callback            |
     |                                       | http://SP.net/<login_cb>?ticket=.     |
     |                                       |<--------------------------------------+
     |                                       |                                       |
.....|.......................................|.......................................|......................................................................
     |                                       |                                       |
     | request SP callback                   |                                       |
     | http://SP.net/<login_cb>?ticket=.     |                                       |
  8. |<--------------------------------------+                                       |
     |                                       |                                       |
     | ticket: e859359ba90c4                 |                                       |
     |                                       |                                       |
     | LOAD   {salt: swid}                   |                                       |
     | REMOVE {salt: swid}                   |                                       |
     |                                       |                                       |
     | salt: 649cfd4101                      |                                       |
     | swid: user@IdP.net                    |                                       |
     |                                       |                                       |
     | validate request: http://IdP.net/swid/validate?salt=.&ticket=.                | SSL/TLS
  9. +---------------------------------------|-------------------------------------->|
     |                                       |                                       | salt: 649cfd4101
     |                                       |                                       | ticket: e859359ba90c4
     |                                       |                                       |
     |                                       |                                       | LOAD   {salt: IdP_salt} ->
     |                                       |                                       |     IdP_salt: d64f9c01
     |                                       |                                       | REMOVE {salt: IdP_salt}
     |                                       |                                       | LOAD   {ticket: [hash, user_id, user, base]} ->
     |                                       |                                       |     hash: hash
     |                                       |                                       |     user_id: user_id
     |                                       |                                       |     user: user
     |                                       |                                       |     base: http://SP.net/
     |                                       |                                       | REMOVE {ticket: [hash, user_id, user, base]}
     |                                       |                                       |
     |                                       |                                       | (hash == hash(user_id, salt, IdP_salt [, IP, ...])) ? OK : Error
     |                                       |                                       |
     |                                       |                                       | OK => load_data(user, base) -> {user=[user], name=[User, Name], mail=[user@email.net]}
     | confirm/reject validation             |                                       |
     | response: user urlencoded data        |                                       | SSL/TLS
 10. |<--------------------------------------|---------------------------------------+
     |                                       |                                       |
     | user_data: {user=[user], name=[User, Name], mail=[user@email.net]}            |
     |                                       |                                       |
     | REMOVE {salt: swid}                   |                                       |
     |                                       |                                       |
     | validation OK ->                      |                                       |
     |    SESSION SAVE: swid (logged user)   |                                       |
     |                                       |                                       |
     | show success/fail login to user       |                                       |
 11. +-------------------------------------->|                                       |
     |                                       |                                       |

     .                                       .                                       .
     .                                       .                                       .
     .                                       .                                       .

     |                                       |                                       |
     | http://SP.net/<logout>                |                                       |
     |<--------------------------------------|                                       |
     |                                       |                                       |
     | redirect SP to UA logout swid (@IdP.net)                                      |
     | redirect http://IdP.net/swid/logout?callback=.                                |
 12. +-------------------------------------->|                                       |
     |                                       | request UA to IdP logout              |
     |                                       | http://IdP.net/swid/logout?callback=. |
 13. |                                       +-------------------------------------->| callback: http://SP.net/<login_cb>
     |                                       |                                       | LOGOUT user - IdP logout
     |                                       | redirect IdP to UA logout callback    |
     |                                       | redirect http://SP.net/<logout_cb>    |
 14  |                                       |<--------------------------------------|
     | request UA to SP logout callback      |                                       |
     | request http://SP.net/<logout_cb>     |                                       |
 15  |<--------------------------------------|                                       |
     | LOGOUT swid - SP logout               |                                       |
     |                                       |                                       |




  1. Používateľ vyplní v UA prihlasovací (login) formulár, kde uvedie svoj SWID
     identifikátor vo formáte user@host[:port]

  2. SP zašle na server IdP požiadavku na prihlásenie - asociácia relácie.
     Jedná sa o GET požiadavku v tvare:

     http://IdP.net/swid/associate?user=.&base=.&callback=.&salt=.

     user --
         Používateľské meno (login) bez domény

     base --
         Základná adresa SP, webovej aplikácie. Tým sa SP identifikuje u IdP.
         IdP môže na základe tejto identifikácie vybrať používateľov
         profil zodpovedajúci danému SP (web aplikácii), který sa použije
	 pre výmenu osobných údajov.
         Doporučuje sa aby _base_ byla obsiahnutá v _callback_, inak môže
         IdP vyhodnotiť SP ako podvodníka a autentifikáciu nepovoliť.

     callback --
         Absolútna URL, kam bude UA (používateľ) presmerovaný po úspešnej
         autentifikácii u IdP.

     salt --
         Náhodná sekvencia znakov. SP musí túto sekvenciu udržovať po
         celú dobu relácie, pretože s ňou bude overovať výsledok celej
         relácie. IdP spravidla tento reťazec použije na vytvorenie _hash_,
         který následne identifikuje reláciu u UA. Základom _hash_ je okrem
         _salt_ tiež napríklad IP adresa SP, alebo používateľské meno.
         IdP tiež pridáva vlastný (tajný) _salt_.

  3. Odpoveďou IdP je spravidla správa obsahujúca položku _auth_.
     V tejto položke je uložená adresa stránky, kam má SP presmerovať UA,
     aby prebehla autentifkácia u IdP. Obsah odpovede IdP je vždy kódovaný
     ako application/x-www-form-urlencoded. Znaková sada je vždy UTF-8.

  4. SP presmeruje UA (HTTP status 301) na authentifikačnú URL IdP.

  5. UA zrealizuje presmerovanie na IdP, a používateľovi je ponúknutý
     prihlasovací formulár alebo iná forma authentifikácie.

  6. V tejto fáze sa používateľ autentifikuje na IdP. Autentifikácia môže
     predstavovať ľubovoľné množstvo krokov. Celý proces by však nemal
     zabrať mnoho času, spravidla maximálně toľko, koľko trvá session na
     SP. Doporučuje sa desiatky minút.

  7. Pokiaľ bolo prihlásenie úspešné, je UA presmerovaný na adresu,
     ktorú SP uviedol v bode 2 ako _callback_.
     IdP k URL pripojí parameter _ticket_:

     ticket --
         Reťazec náhodných znakov, ktoré sa použijú k následnému
         overeniu relácie na strane SP.

  8. UA zrealizuje presmerovanie na SP.

  9. SP musí validovať reláciu na základe parametrov _salt_ a _ticket_ na URL:

     http://IdP.net/swid/validate?salt=.&ticket=.

     salt --
          Rovnaký reťazec ako v bode 2.

     ticket --
          Hodnota premennej ticket v bodě 7.

  10. Výsledkom úspešne overenej relácie je správa, opäť kódovaná ako
      application/x-www-form-urlencoded a znaková sada UTF-8, obsahujúca
      osobné údaje, ktoré sa používateľ rozhodol poskytnúť SP. Obsah ani
      rozsah osobných údajov nie je v protokole definovaný. Protokol
      definuje iba položky, ktoré majú jednotný význam. Ich popis bude
      uvedený nižšie.

  11. SP zobrazí používateľovi výsledok úspešnej autentifikace.

  ...

  12. Odhlásenie.  TODO !!!

