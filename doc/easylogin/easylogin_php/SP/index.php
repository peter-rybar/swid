<?php

require_once "easylogin.php";

if (isset($_POST["easyloginid"])) {

  $id = $_POST["easyloginid"];                        
  setcookie("saveid",$id,time()+365*24*3600);
  $returnUrl = easyLogin_curUrlBase(true);
  session_start();
	$salt = uniqid("");
	$_SESSION["salt"] = $salt;
	$_SESSION["loginId"] = $id;
 	session_write_close();
	$error = easyLogin_login($id, "http://".$_SERVER["HTTP_HOST"], $returnUrl, $salt);
} else if (isset($_GET["status"])) {

  $status = $_GET["status"];
  if ($status == "ok") {
    $ticket = $_GET["ticket"];
    session_start();    
    $validationResult = easyLogin_validateResponse($_SESSION["loginId"],$_SESSION["salt"]);
    if (is_array($validationResult)) {
      $showResult = $validationResult;
    } else {
      $error = $validationResult;
    }
  }
  
}


header("Content-type: text/html; charset=utf-8");

?>

<html>
<head>
<title>EasyLogin - TryIt page</title>
</head>
<body style="background-color: black; color:yellow;">
<h1 style="color:#ccc">easylogin-test.novacisko.cz</h1>
<?php
if (isset($showResult)) {
    echo "<table border=\"1\">";
    echo "<tr><td>Your ID saved by Service Provider:</td><th>",htmlspecialchars($_SESSION["loginId"]),"</th></tr>";
    foreach($showResult as $k=>$v) {
      echo "<tr><td>$k</td><th>",htmlspecialchars($v),"</th></tr>";
    }
    echo "</table>";
    echo "<hr>";
}
if (isset($error)) echo "<div style=\"color:red;font-weight:bold\">",$error,"</div>";
?>
<form action="?" method="POST">
<label>Enter your EasyLogin-ID: <input type="text" name="easyloginid" value="<?php echo htmlspecialchars(@$_COOKIE["saveid"]); ?>"></label>
<input type="submit" value="Login">
</form>
</body>
</html>

