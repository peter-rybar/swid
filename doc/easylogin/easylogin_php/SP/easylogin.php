<?php

define("EASYLOGIN_ERR_RESOLVE_FAILED","Cannot resolve address of login server");
define("EASYLOGIN_ERR_SERVER_LOGIN_FAILED","Unable to login to provider's server");
define("EASYLOGIN_ERR_SERVER_INVALID_RESPONSE","Unable parse server response");
define("EASYLOGIN_ERR_SERVER_VALIDATION_NOT_ACCEPTABLE","IdP returns unacceptable 'profileId'");
define("EASYLOGIN_ERR_SERVER_VALIDATE_FAILED","Unable to validate login on provider's server");


///Retrieves URL of service using DNS record
/**
 * @param string $domain domain name 
 * @return string url for the service. Note that if record is not found, it returns http://easylogin.domain.xxx
 */  
function easyLogin_getServiceUrl($domain) {
	
	if ($domain == "localhost") return "http://$domain/";
	
	$dnsres = @dns_get_record("_easylogin._tcp.$domain",DNS_SRV);

	if ($dnsres===FALSE || count($dnsres) < 1) {
		$hostname="easylogin.$domain";
	} else {
		$rnd = rand(0,count($dnsres));
		$rec = $dnsres[$rnd];
		$hostname=$rec["target"].":".$res["port"];		
	} 
	
	return "http://".$hostname."/";
}

 
///Retrieves URL of current directory or page
/**
 * @param string withFilename appends current script name after the directory
 * @return url of current directory or currently running script. 
 * 
 * Useful to build redirects.  
 */  

function easyLogin_curUrlBase($withFilename = false) {
	
	$https = isset($_SERVER["https"]) && $_SERVER["https"] != "" && $_SERVER["https"] != "off";
	if ($https) $proto="https://"; else $proto = "http://";
	$hostname= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"];
	$uri = $_SERVER["REQUEST_URI"];
	$a = strpos($uri,"?"); 
	if ($a !== FALSE) 
		$url = substr($uri,0,$a);
	else
		$url = $uri;

	if (!$withFilename) {
		$a = strrpos($url,"/");
		if ($a !== FALSE) 
			$base = substr($url,0,$a);
		else 
			$base = "/";
	} else {
		$base = $url;
	}
		
	return $proto.$hostname.$base;
}

///Validates name of domain
/** 
 * @param string $domain string contains domain name
 * @retval true domain should be valid
 * @retval false domain contains invalid characters
 */   
function easyLogin_validateDomain($domain) {
	
	return ereg("[-_a-zA-Z0-9.]+",$domain);	
}

///Parsers response from identity provider
/**
 * @param resource $fd reply of IdP as file
 * @return parsed reply as associated array 
 */  
function easyLogin_parseProviderResponse($fd) {
	
	$contents = stream_get_contents($fd);
	$res = array();	
	parse_str($contents,$res);
	return $res;
}

///Splits EasyLogin-ID to username and domain
/** 
 * @param string $id contain EasyLogin-ID
 * @return array with two items, where first one is username and second one is domain
 * 
 * @note It splits string on the last @. You are able to chain ID's for 
 * example by this way; user@domain1.xxx@domain2.xxx@domain3.xxx.
 * On first resolve, it will cut out domain3.xxx giving the server
 * chance to resolve remaining part's of username
 * 
 */
function easyLogin_splitId($id) {
	$pos = strrpos($id,"@");
	if ($pos === FALSE) return array("",$id);
	else return array(substr($id,0,$pos),substr($id,$pos+1));
}
///Converst ID to address of login server and final id
/**
 * @param string  $id contains EasyLogin-ID in form name@domain.xxx
 * @retval array contains two fields. "user" and "loginUrl". User contains
 * name of "user" to autentificate onto IdP and "loginUrl" contains URL where
 * to sned login request.
 * @retval false cannot resolve domain 
 */    
function easyLogin_resolveService($id) {
	$url = "";
	$user = "";
	$probeParams = "";
	for ($i = 0; $i < 10; $i++) {
		if ($url == "") {					
			list($user,$domain) = easyLogin_splitId($id);
			if (easyLogin_validateDomain($domain) == false) 
					return false;
			$tryurl = easyLogin_getServiceUrl($domain);
			$probeParams = "?".http_build_query(array("action"=>"probe","user"=>$user));
			$url = $tryurl;
			$fd = @fopen($url.$probeParams,"r");
			if ($fd === FALSE) {
				$url = "http://$domain/easylogin";
				$fd = @fopen($url.$probeParams,"r");
			}
			if ($fd === FALSE) return FALSE;
		} else {
			$fd = @fopen($url.$probeParams,"r");
		}
		$data = easyLogin_parseProviderResponse($fd);
		if (isset($data["status"]) && $data["status"] == "ok") return array("user"=>$user,"loginURL"=>$url); 
		if (isset($data["alias"])) {
			$url = "";
			$id = $data["alias"]; 			
		} else if (isset($data["loginURL"])) {
			$url = $data["loginURL"];
		} else {
			return FALSE;
		}
	}
}


///Connects IdP and retrieves URL of login form
/**
 * @param string $id string contains ID to log in
 * @param string $base Base URL of Service Provider (SP) to identify SP at IdP
 * @param string $returnUrl URL where UA will be returned after succesfully login
 * @param string $salt Random string. Value must be stored for accessing the result 
 *       on successful login. Every request should have different $slat.
 * @param int $noIPcheck request IdP to do not IP checking on this connection. 
 *       By default IdP may check, whether requests from SP are incoming from
 *       the same IP. If SP cannot handle IP changes between request, it can
 *       disable this feature. Note that disabling IP checking may be less secure.
 * @param string $redirUrl variable is filled with URL to redirect UA.
 * @retval TRUE on success
 * @retval string describes error.   
 */
         
function easyLogin_loginProbe($id, $base, $returnUrl, $salt, $noIPcheck, &$redirUrl) {
			
	$resolved = easyLogin_resolveService($id);
	if ($resolved === FALSE) return EASYLOGIN_ERR_RESOLVE_FAILED;
	$request = $resolved["loginURL"]."?".http_build_query(
		array("action"=>"login",
			  "returnUrl"=>$returnUrl,
			  "user"=>$resolved["user"],
			  "base"=>$base,
			  "salt"=>$salt,
        "noIPcheck"=>$noIPcheck));
	

	$fd = @fopen($request,"r");
	if ($fd === FALSE) return EASYLOGIN_ERR_SERVER_LOGIN_FAILED;
	$data = easyLogin_parseProviderResponse($fd);
	fclose($fd);
	
	
	if (!isset($data["loginPageURL"])) 
		return EASYLOGIN_ERR_SERVER_INVALID_RESPONSE;
	
	$redirUrl = $data["loginPageURL"];
	if (substr($redirUrl,0,7) != "http://" &&
		substr($redirUrl,0,8) != "https://")  
			return EASYLOGIN_ERR_SERVER_INVALID_RESPONSE;
	
	return TRUE;			
}

///Perofrms redirect
/**
 * @param string $url url to redirect
 * @return function will not return.  
 * 
 * @note you have to call function before output starts
 */    
function easyLogin_redirect($url) {
		header("Location: ".$url);
		echo "<html><body>";
		echo "<a href=\"",htmlspecialchars($url),"\">→</a>";
		echo "</body></html>";
		die();			
}

///Attempt login using EasyLogin-ID
/**
 * Discover and access IdP login server and redirect UA to the IdP's login form
 * 
 * @param string $id ID to log in
 * @param string $base Base URL of Service Provider (SP) to identify SP at IdP
 * @param string $returnUrl URL where UA will be returned after succesfully login
 * @param string $salt Random string. Value must be stored for accessing the result 
 *       on successful login. Every request should have different $slat.
 * @param int $noIPcheck request IdP to do not IP checking on this connection. 
 *       By default IdP may check, whether requests from SP are incoming from
 *       the same IP. If SP cannot handle IP changes between request, it can
 *       disable this feature. Note that disabling IP checking may be less secure.
 * @return On success, function will not return. On failure, result is string contains an error message */
 
 function easyLogin_login($id, $base, $returnUrl, $salt,$noIPcheck = false) {
	$redirUrl = "";
	$res = easyLogin_loginProbe($id, $base, $returnUrl, $salt, $noIPcheck, $redirUrl);
	if ($res === TRUE) {
		easyLogin_redirect($redirUrl);
	} else {
		return $res;
	}	
}


///Creates URL for validation request
function easyLogin_validateResponseUrl($id,$salt,$noIPcheck = false) {
	$resolved = easyLogin_resolveService($id);
	if ($resolved === FALSE) return FALSE;
	
	$request = $resolved["loginURL"]."?".http_build_query(
		array("action"=>"validate",
			  "ticket"=>$_GET["ticket"],
			  "salt"=>$salt,
        "noIPcheck"=>$noIPcheck));
	return $request;
}
///Validates response 
/**
 * Function must be called when UA is redirected to returnURL. Function retrieves
 * parameter ticked from the GET request and sends validation request to
 * the IdP's login server.
 * 
 * @param string $id ID of user which used with easyLogin_login
 * @param string $salt salt used with easyLogin_login
 * @param int $noIPcheck same as easyLogin_login, must have same value
 * @return user profile as associative array
 */

function easyLogin_validateResponse($id,$salt,$noIPcheck = false) {
	
	$request = easyLogin_validateResponseUrl($id,$salt,$noIPcheck);
	if ($request === FALSE) return EASYLOGIN_ERR_RESOLVE_FAILED;
	
	$fd = fopen($request,"r");	
	if ($fd === FALSE) return EASYLOGIN_ERR_SERVER_VALIDATE_FAILED;
	$data = easyLogin_parseProviderResponse($fd);
	fclose($fd);
  
  $profileID = $data["profileId"];
  $domainpos = strrpos($profileID, "@");
  if ($domainpos === FALSE) return EASYLOGIN_ERR_SERVER_VALIDATION_NOT_ACCEPTABLE;
  
  $domain = substr($profileID,$domainpos+1);
  $domainfromurl = parse_url($request,PHP_URL_HOST);
  if ($domainfromurl != $domain && substr($domainfromurl,-strlen($domain)-1) != ".$domain")
      return EASYLOGIN_ERR_SERVER_VALIDATION_NOT_ACCEPTABLE;
  	
	return $data;		
}


?>