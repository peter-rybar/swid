<?php

///Reports error as HTTP error code
/** @param int $code error code
 *  @param string $message error message
 */ 
function easyLogin_error($code, $message) {

  header("HTTP/1.0 $code $message");
  die();

}

///Appends paramaters to an URL
/**
 * @param string $url source url
 * @param string $params parameters asi query string
 * @return  url with parameters  
 */ 
function easyLogin_appendParams($url, $params) {
	if (strpos($url,"?") === FALSE)
		return $url."?".$params;
	else 
		return $url."&".$params;
}

///builds probe response
function easyLogin_handleProbe() {
	echo http_build_query(array("status"=>"ok"));
}

///handles login request
/**
 * @param string $loginFormUrl base URL for login form. Function will append required parameters
 * @param string $serverSalt random string used to create HASH-ID. Server salt should change time to time, but must be same during
 *        whole login session. Because requests from SP are stateless, it is hard to implement creation unique salt for
 *        each login session without transfering the salt through UA (which is dangerous). 
 *        Changing salt for example each per day should be enought. Specify static salt is less secure, but sometime is also enought
 *        
 * Function doesn't return. Result is sent to the standard output. It contains loginPageUrl response with URL to login form service
 */      
function easyLogin_handleLogin($loginFormUrl,$serverSalt) {
  $returnUrl = $_GET["returnUrl"];
  $user=$_GET["user"];
  $base=$_GET["base"];
  $salt=$_GET["salt"];
  $noIPCheck=$_GET["noIPcheck"];
  $addr = $noIPCheck?"":$_SERVER['REMOTE_ADDR'];
  
  $hash=MD5($addr.$salt.$user.$serverSalt);
  
  $redirUrl = easyLogin_appendParams($loginFormUrl,
  			http_build_query(
  				array("hash"=>$hash,
  					  "user"=>$user,
  					  "returnUrl"=>$returnUrl,
  					  "base"=>$base)));
  
  echo http_build_query(array("loginPageURL"=>$redirUrl));
  die();
}

///handles validation request
/** @param string $serverSalt same salt used in easyLogin_handleLogin
 *  @param function $getUserInfoCallback function to retrieve user profile
 *  
 *  Validates ticket posted using GET request from SP. If validation is successful, returns user profile to the standard output  
 */ 
function easyLogin_handleValidation($serverSalt,$getUserInfoCallback) {
  
  unset($_COOKIE[session_name()]);
  session_name("EasyLoginSession");	
  session_id($_GET["ticket"]);
  $salt=$_GET["salt"];
  $noIPCheck=$_GET["noIPcheck"];
  $addr = $noIPCheck?"":$_SERVER['REMOTE_ADDR'];
  
  session_start();
  if (!isset($_SESSION["easylogin.profile"]) || !isset($_SESSION["easylogin.hash"])) 
  		easyLogin_error(404,"Not found");
  
  $profile=$_SESSION["easylogin.profile"];
  $requser=$_SESSION["easylogin.requser"];
  $hash=MD5($addr.$salt.$requser.$serverSalt);    
                                                                    
                                  
  if ($hash != $_SESSION["easylogin.hash"]) 
//        $userinfo = array('name'=>"$hash - ".$_SESSION["easylogin.hash"]);
    		easyLogin_error(403,"Forbidden - $hash != ".$_SESSION["easylogin.hash"]);
  else
    $userinfo=$getUserInfoCallback($profile);
  
  if (!isset($userinfo["profileId"])) {
      easyLogin_error(500,"Error at IdP - profileId is not set");
  }
  
  unset($_SESSION["easylogin.hash"]);
  echo http_build_query($userinfo);  
	session_write_close();
}

///Handles all requests from SP.
/**
 * @param string $loginFormUrl URL of login form (without additional parameters)
 * @param string $serverSalt self defined by server
 * @param function $getUserInfoCallback function to fetch user profile
 * 
 * function doesn't return. It responses the request depend on action in query
 */     
function easyLogin_processRequestFromRelay($loginFormUrl,$serverSalt, $getUserInfoCallback) {
	
	if (!isset($_GET["action"])) easyLogin_error(400,"Invalid request");
	else if ($_GET["action"] == "probe") easyLogin_handleProbe();
	else if ($_GET["action"] == "login") easyLogin_handleLogin($loginFormUrl,$serverSalt);
	else if ($_GET["action"] == "validate") easyLogin_handleValidation($serverSalt, $getUserInfoCallback);
	else easyLogin_error(400,"Invalid request");
	
}

///Call this function when user is successfuly autenticated and IdP requires to redirect user back to the SP endpoint URL
/**
 * @param string $returnUrl SP's endpoint (URL). This value is delivered through UA when it requests login form. 
 * @param string $user User ID. This value is delivered through UA when it requests login from. 
 *       You must verbatin the field from the request to login form even if user identification 
 *        has been changed during autentification process. Field is used to calculate validation hash and
 *        any changes causes that session becomes invalid
 * @param string $profile name of profile of the user. Parameter is passed to the "getUserInfoCallback". It should be string
 * @param string $hash hash string. This value is delivered through UA when it requests login form
 *                                                                                                  
 * Function never returns and redirects UA to SP's endpoint.
 */           
function easyLogin_loginSuccess($returnUrl, $requser, $profile, $hash) {
  session_write_close();
  session_name("EasyLoginSession");	
  session_id("");                 
 	session_start();                                  
 	$_SESSION["easylogin.hash"] = $hash;
 	$_SESSION["easylogin.requser"] = $requser; 	
 	$_SESSION["easylogin.profile"] = $profile; 	
	$query = http_build_query(array("status"=>"ok","ticket"=>session_id())); 
	$url = easyLogin_appendParams($returnUrl, $query);
 	header("location: $url");
	session_write_close();
 	die(); 
}

///Call this function to reject login.
/**
 * Use this function, if user wish to cancel login request
 * 
 * @param string $returnUrl SP's endpoint (URL). This value is delivered through UA when it requests login form.                                                                                     
 * Function never returns and redirects UA to SP's endpoint.
 */
   
function easyLogin_loginRejected($returnUrl) {
		
	$query = http_build_query(array("status"=>"rejected"));
	$url = easyLogin_appendParams($returnUrl, $query);
 	header("location: $url");
 	die(); 
}
?>